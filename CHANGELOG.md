- 1.8.0 (23 Dec. 2024): Updated for 1.21.4
- 1.7.0 (29 Nov. 2024):
  - Updated for 1.21.2-1.21.3
  - Added optional `"removal_behavior"` field to source patterns as an alternative to `"remove"` and `"always_remove"`:
    - `"removal_behavior": "normal"` is equivalent to `"remove": true, "always_remove": false` (default)
    - `"removal_behavior": "never"` is equivalent to `"remove": false, "always_remove": false`
    - `"removal_behavior": "always"` is equivalent to `"remove": true, "always_remove": true`
    - `"normal"` means a recipe will only be removed if it helps generate a result recipe
    - `"remove"` and `"always_remove"` will be **removed** in the first release supporting Minecraft 1.22
  - Substantial internal changes
- 1.6.1 (3 Nov. 2024): Ported 1.5.1 to MC 1.21-1.21.1
- 1.5.1 (3 Nov. 2024): Ported 1.4.1 to MC 1.20.5-1.20.6
- 1.4.1 (3 Nov. 2024): Ported 1.3.1 to MC 1.20.3-1.20.4
- 1.3.1 (3 Nov. 2024):
  - Fixed an issue that could change recipes that were recognized but not removed so their output count became 1
  - Fixed matching empty space in shaped recipes
  - Fixed generation of shapeless recipes with duplicate ingredients
  - Thanks to Noobulus for reporting these and previous issues, helping to improve the mod
- 1.6.0 (21 Oct. 2024): Ported 1.5.0 to MC 1.21-1.21.1
- 1.5.0 (21 Oct. 2024): Ported 1.4.0 to MC 1.20.5-1.20.6
- 1.4.0 (21 Oct. 2024): Ported 1.3.0 to MC 1.20.3-1.20.4
- 1.3.0 (21 Oct. 2024):
  - Backported changes from 1.2.2 to MC 1.20.2
  - Fixed `common_ingredients` for matching shapeless recipes
  - Fixed `id_substrings` for matching shapeless recipes
- 1.2.2 (4 Sep. 2024):
  - Marked as compatible with 1.21.1
  - Fixed recipes from conflict_resolution and fixed_inconsistencies not actually being added
  - Updated recipe removal to use pack.mcmeta filters instead of overwriting and outputting air  
    ("Item must not be minecraft:air" errors will no longer be replaced with "Removing recipe: some:recipe" messages)
- 1.2.1 (21 Jul. 2024):
  - Fixed a crash/data pack error caused by generating recipes with different types but the same ids
  ([#5](https://gitlab.com/supersaiyansubtlety/recipe_reshaper/-/issues/5))
  - Added optional [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates (client-side)
- 1.2.0 (17 Jun. 2024):
  - Updated for 1.21
  - Updated translations
  - Minor internal changes
- 1.1.8 (10 May 2024):
  - Marked as compatible with 1.20.6
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
- 1.1.7 (27 Apr. 2024): Updated for 1.20.5
- 1.1.6 (2 Feb. 2024): Updated for 1.20.3 and 1.20.4
- 1.1.5 (14 Nov. 2023): Updated for 1.20.2
- 1.1.4 (21 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.1.3 (17 May 2023): Ported a fix for a crash that could result from generating invalid ids when trying to resolve
recipe id conflicts
- 1.1.2 (11 May 2023): Ported fixes for missing recipes from 1.19.2
- 1.1.1 (25 Apr. 2023): Ported compatibility fix with QFAPI 4.0.0-beta.27+
- 1.1.0 (1 Apr. 2023):

  - Updated for 1.19.4
  - Added optional "group" specification for resulting recipes
    - this works the same way as it does for vanilla recipes when specified
    - if not specified, the generated recipes' output items will be used as those recipes' groups
    - to specify no group, set group to the empty string ("")
  - added optional "category" specification for resulting recipes
    - this works the same way as it does for vanilla recipes
    - vanilla categories are "building", "redstone", "equipment", and "misc"
    - if not specified, recipes will be categorized as "misc"

- 1.0.6 (6 Aug. 2022): Marked as compatible with 1.19.2
- 1.0.5 (3 Aug. 2022): Fixed log spam on recipe removal
- 1.0.4 (29 Jul. 2022):
  - Marked as compatible with 1.19.1
  - Minor internal changes
- 1.0.3 (14 Jul. 2022):
  - Fixed an issue that prevented reshaped recipes from appearing in the recipe book
  - May also fix #1
- 1.0.2 (30 Jun. 2022): Updated for 1.18.2 ***and*** 1.19!
- 1.0.1 (13 Dec. 2021): Updated for 1.18.1
- 1.0 (3 Dec. 2021): 

  Updated for 1.18!

  First full release!

- 0.1-b (11 Oct. 2021): 
  
  - greatly simplified internals by using the same character tokens used in the JSON
  - renamed all 'input' classes and elements to 'source'
  - made normal recipe removal less aggressive so that recipes that don't contribute to a new recipe won't be removed
  - added optional `always_remove` parameter to source patterns which makes them remove any recipe that matches
  regardless of whether or not it contributes to a new recipe
  - added mod icon and mcmeta files with descriptions for the builtin datapacks
  
- 0.0.3-a (9 Oct. 2021):

  - implemented shapeless inputs and results
  - added more_buttons_recipe_reshaper
  - added 2x2_logs_to_wooden_slabs_and_stairs_recipe_reshaper
  
- 0.0.2-a (1 Oct. 2021):
  
  - fixed chiseled recipes in conflict_resolver
  - made all input stacks in ingredients have count 1
  - made common_ingredients affect outputs
  - removed unused classes

- 0.0.1-a (1 Oct. 2021):
  
  First alpha version. 
  
  implemented:
  - shaped input patterns
  - shaped result patterns
  - common ingredients
  - id substring matching
    
  todo:
  - handle tag ingredients becoming outputs better: try to infer the 'base' ingredient
  - support matching shapeless recipes
  - support outputting shapeless recipes
  - add 'export datapack' feature to 'bake' generated recipes from a reshaper into static, vanilla recipes
  - (maybe) add id_regex