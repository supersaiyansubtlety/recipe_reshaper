package net.sssubtlety.recipe_reshaper.reshaper.pattern;

import net.sssubtlety.recipe_reshaper.mixin.accessor.ShapedRecipePatterDataAccessor;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import net.minecraft.recipe.Ingredient;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static net.sssubtlety.recipe_reshaper.reshaper.pattern.AbstractRecipePattern.CommonData.crossReferenceTokenSet;
import static net.sssubtlety.recipe_reshaper.reshaper.Reshaper.EMPTY_TOKEN;
import static net.sssubtlety.recipe_reshaper.util.StringUtil.lineJoin;

public abstract class ShapedPattern extends AbstractRecipePattern {
    protected final Character[] pattern;
    public final int width;
    public final int height;

    public ShapedPattern(
        Character[] pattern, Character outputToken, int outputCount,
        int width, int height,
        ImmutableMap<Character, Ingredient> commonIngredients
    ) {
        // pattern maps position in crafting grid to token
        super(outputToken, outputCount, commonIngredients);

        this.height = height;
        this.width = width;
        this.pattern = pattern;
    }

    public static final class Data {
        private static final MapCodec<ImmutableList<String>> PATTERN_CODEC =
            ShapedRecipePatterDataAccessor.recipe_reshaper$getPATTERN_CODEC()
                .xmap(ImmutableList::copyOf, Function.identity())
                .fieldOf(Keys.PATTERN);

        // source patterns allow output count 0 which matches any count
        // result patterns require an output count of at least 1
        public static MapCodec<Data> codecOf(MapCodec<CommonData> commonCodec) {
            return Codec.mapPair(commonCodec, PATTERN_CODEC).xmap(
                commonAndPattern -> new Data(commonAndPattern.getFirst(), commonAndPattern.getSecond()),
                shaped -> new Pair<>(shaped.common, shaped.pattern)
            );
        }

        private final CommonData common;
        private final ImmutableList<String> pattern;
        private final int width;
        private final int height;

        private Data(CommonData common, ImmutableList<String> pattern) {
            this.common = common;
            this.pattern = pattern;

            // PATTERN_CODEC validates column and row dimensions
            this.width = pattern.getFirst().length();
            this.height = pattern.size();
        }

        public CommonData common() {
            return this.common;
        }

        public int width() {
            return this.width;
        }

        public int height() {
            return this.height;
        }

        public DataResult<Character[]> createFlatPattern(Set<Character> tokenSet, boolean expandSet) {
            // Assign each string token a unique integer and store those integers in a flattened array.
            // Mappings are consistent amongst patterns in this family.
            final int numIngredients = this.width * this.height;
            final Character[] flatPattern = new Character[numIngredients];
            final List<String> errors = new LinkedList<>();
            for (int col = 0; col < this.height; col++) {
                for (int row = 0; row < this.width; row++) {
                    final char token = this.pattern.get(col).charAt(row);

                    if (token != EMPTY_TOKEN) {
                        crossReferenceTokenSet(tokenSet, expandSet, token)
                            .ifPresent(errors::add);
                    }

                    flatPattern[col * this.width + row] = token;
                }
            }

            crossReferenceTokenSet(tokenSet, expandSet, this.common.outputToken())
                .ifPresent(errors::add);

            if (errors.isEmpty()) {
                return DataResult.success(flatPattern);
            } else {
                return DataResult.error(() -> lineJoin(errors));
            }
        }
    }
}
