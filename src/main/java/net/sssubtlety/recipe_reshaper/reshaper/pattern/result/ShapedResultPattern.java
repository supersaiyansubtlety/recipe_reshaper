package net.sssubtlety.recipe_reshaper.reshaper.pattern.result;

import com.google.common.collect.ImmutableMap;

import net.minecraft.recipe.CraftingCategory;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.recipe.ShapedRecipe;
import net.minecraft.recipe.ShapedRecipePattern;
import net.minecraft.util.Identifier;

import net.sssubtlety.recipe_reshaper.mixin.accessor.ShapedRecipeAccessor;
import net.sssubtlety.recipe_reshaper.util.Emptyable;
import net.sssubtlety.recipe_reshaper.reshaper.pattern.ShapedPattern;
import net.sssubtlety.recipe_reshaper.reshaper.mapping.IngredientMapping;
import net.sssubtlety.recipe_reshaper.util.StringUtil;

import org.jetbrains.annotations.Nullable;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static net.sssubtlety.recipe_reshaper.RecipeReshaper.NAMESPACE;
import static net.sssubtlety.recipe_reshaper.util.StringUtil.LINE_TAB;
import static net.sssubtlety.recipe_reshaper.util.StringUtil.toPrintableString;

import static java.lang.System.lineSeparator;

public final class ShapedResultPattern extends ShapedPattern implements ResultPattern<ShapedRecipe> {
    private final String idSuffix;
    @Nullable
    private final String group;
    private final CraftingCategory category;

    private ShapedResultPattern(
        Character[] flatPattern, int width, int height, Character outputToken, int outputCount,
        ImmutableMap<Character, Ingredient> commonIngredients, String idSuffix,
        @Nullable String group, CraftingCategory category
    ) {
        super(flatPattern, outputToken, outputCount, width, height, commonIngredients);

        this.idSuffix = idSuffix;
        this.group = group;
        this.category = category;
    }

    @Override
    public String getIdSuffix() {
        return this.idSuffix;
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.ofNullable(this.group);
    }

    @Override
    public CraftingCategory getCategory() {
        return this.category;
    }

    @Override
    public int getOutputCount() {
        return this.outputCount;
    }

    @Override
    public Identifier getDescriptiveId(ShapedRecipe recipe) {
        // shaped_<W>x<H>_<ingredients>_to_<count>_<output>
        return Identifier.of(NAMESPACE, "shaped_%sx%s_%s_%s".formatted(
            recipe.getWidth(), recipe.getHeight(),
            StringUtil.idPathOf(recipe.getIngredients().stream()),
            ResultPattern.toDescriptiveIdSuffix(
                ((ShapedRecipeAccessor) recipe).recipe_reshaper$getResult()
            )
        ));
    }

    @Override
    public Optional<RecipeHolder<ShapedRecipe>> tryGenerateRecipe(
        IngredientMapping<?> ingredientMapping, Identifier reshaperId
    ) {
        final int numIngredients = this.pattern.length;
        final List<Optional<Ingredient>> ingredients = new ArrayList<>(numIngredients);
        final Map<Character, Optional<Ingredient>> resolvedPattern = new HashMap<>();

        for (final char token : this.pattern) {
            final Optional<Emptyable<Ingredient>> resolvedIngredient =
                this.resolveIngredient(ingredientMapping, token);

            if (resolvedIngredient.isPresent()) {
                final Optional<Ingredient> ingredient = resolvedIngredient.get().get();
                ingredients.add(ingredient);
                resolvedPattern.put(token, ingredient);
            } else {
                return Optional.empty();
            }
        }

        return this.tryGenerateRecipe(
            ingredientMapping, reshaperId,
            (group, category, output) -> {
                final ShapedRecipePattern recipePattern =
                    new ShapedRecipePattern(this.width, this.height, ingredients, Optional.empty());
                return new ShapedRecipe(group, category, recipePattern, output);
            },
            () -> this.representInput(resolvedPattern)
        );
    }

    private CharSequence representInput(Map<Character, Optional<Ingredient>> resolvedPattern) {
        final StringBuilder representation = new StringBuilder("pattern: [");

        for (int row = 0; row < this.height; row++) {
            representation.append(LINE_TAB);
            for (int col = 0; col < this.width; col++) {
                representation.append(this.pattern[row * this.width + col]);
            }
        }

        representation
            .append(lineSeparator())
            .append(']')
            .append(lineSeparator())
            .append("key: {");

        resolvedPattern.forEach((token, optIngredient) -> optIngredient.ifPresent(ingredient ->
            representation
                .append(LINE_TAB)
                .append('"')
                .append(token)
                .append("\": ")
                .append(toPrintableString(ingredient))
        ));

        representation
            .append(lineSeparator())
            .append("}");

        return representation;
    }

    public record Data(ShapedPattern.Data shaped, ResultPattern.Data result) implements ResultPattern.Assembler {
        public static final MapCodec<Data> CODEC = Codec.mapPair(
            ShapedPattern.Data.codecOf(ResultPattern.Data.COMMON_CODEC),
            ResultPattern.Data.CODEC
        ).xmap(
            shapedAndResults -> new Data(shapedAndResults.getFirst(), shapedAndResults.getSecond()),
            shapedResult -> new Pair<>(shapedResult.shaped, shapedResult.result)
        );

        @Override
        public DataResult<ShapedResultPattern> assemble(
            Set<Character> tokenSet, ImmutableMap<Character, Ingredient> commonIngredients
        ) {
            return this.shaped.createFlatPattern(tokenSet, false)
                .map(pattern -> new ShapedResultPattern(
                    pattern,
                    this.shaped.width(), this.shaped.height(),
                    this.shaped.common().outputToken(), this.shaped.common().outputCount(),
                    commonIngredients,
                    this.result.idSuffix(), this.result.group().orElse(null), this.result.category()
                ));
        }
    }
}
