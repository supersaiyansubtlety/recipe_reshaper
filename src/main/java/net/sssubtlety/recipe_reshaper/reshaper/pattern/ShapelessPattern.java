package net.sssubtlety.recipe_reshaper.reshaper.pattern;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.LinkedHashMultiset;
import com.google.common.collect.Multiset;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.dynamic.Codecs;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static net.sssubtlety.recipe_reshaper.reshaper.pattern.AbstractRecipePattern.CommonData.crossReferenceTokenSet;
import static net.sssubtlety.recipe_reshaper.reshaper.Reshaper.EMPTY_TOKEN_STRING;
import static net.sssubtlety.recipe_reshaper.reshaper.Reshaper.EMPTY_TOKEN;
import static net.sssubtlety.recipe_reshaper.util.StringUtil.lineJoin;

public abstract class ShapelessPattern extends AbstractRecipePattern {
    // maps counts to tokens
    protected final ImmutableMultimap<Integer, Character> countPattern;

    public ShapelessPattern(
        ImmutableMultimap<Integer, Character> countPattern, Character outputToken, int outputCount,
        ImmutableMap<Character, Ingredient> commonIngredients
    ) {
        super(outputToken, outputCount, commonIngredients);
        this.countPattern = countPattern;
    }

    public record Data(CommonData common, String pattern) {
        public static final int MAX_PATTERN_ENTRIES = 9;

        private static final MapCodec<String> PATTERN_CODEC =
            Codecs.NON_EMPTY_STRING.validate(string -> {
                    if (string.contains(EMPTY_TOKEN_STRING)) {
                        return DataResult.error(() ->
                            "Invalid pattern entry: '" + EMPTY_TOKEN_STRING + "' (space) is the reserved symbol for " +
                                "empty ingredients; shapeless recipes can't accept empty ingredients"
                        );
                    } else {
                        final int patternLen = string.length();
                        if (patternLen > MAX_PATTERN_ENTRIES) {
                            return DataResult.error(() ->
                                "Invalid pattern: too many entries (" + patternLen + "), " +
                                    MAX_PATTERN_ENTRIES + " is maximum"
                            );
                        } else {
                            return DataResult.success(string);
                        }
                    }
                })
                .fieldOf(Keys.PATTERN);

        public static MapCodec<Data> codecOf(MapCodec<CommonData> commonCodec) {
            return Codec.mapPair(commonCodec, PATTERN_CODEC).xmap(
                commonAndPattern -> new Data(commonAndPattern.getFirst(), commonAndPattern.getSecond()),
                shapeless -> new Pair<>(shapeless.common, shapeless.pattern)
            );
        }

        public DataResult<ImmutableMultimap<Integer, Character>> createCountPattern(
            Set<Character> tokenSet, boolean expandSet
        ) {
            final Multiset<Character> tokens = LinkedHashMultiset.create();
            final List<String> errors = new LinkedList<>();
            for (final char token : this.pattern.toCharArray()) {
                if (token != EMPTY_TOKEN) {
                    crossReferenceTokenSet(tokenSet, expandSet, token)
                        .ifPresent(errors::add);
                }

                tokens.add(token);
            }

            final Set<Multiset.Entry<Character>> charEntries = tokens.entrySet();
            final ImmutableMultimap.Builder<Integer, Character> tokenCounts = ImmutableMultimap.builder();

            for (final Multiset.Entry<Character> entry : charEntries) {
                tokenCounts.put(entry.getCount(), entry.getElement());
            }

            crossReferenceTokenSet(tokenSet, expandSet, this.common.outputToken())
                .ifPresent(errors::add);

            if (errors.isEmpty()) {
                return DataResult.success(tokenCounts.build());
            } else {
                return DataResult.error(() -> lineJoin(errors));
            }
        }
    }
}
