package net.sssubtlety.recipe_reshaper.reshaper.pattern.result;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;

import net.minecraft.recipe.CraftingCategory;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.recipe.ShapelessRecipe;
import net.minecraft.util.Identifier;

import net.sssubtlety.recipe_reshaper.mixin.accessor.ShapelessRecipeAccessor;
import net.sssubtlety.recipe_reshaper.util.Emptyable;
import net.sssubtlety.recipe_reshaper.reshaper.pattern.ShapelessPattern;
import net.sssubtlety.recipe_reshaper.reshaper.mapping.IngredientMapping;
import net.sssubtlety.recipe_reshaper.util.StringUtil;

import org.jetbrains.annotations.Nullable;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.sssubtlety.recipe_reshaper.RecipeReshaper.NAMESPACE;
import static net.sssubtlety.recipe_reshaper.util.StringUtil.toPrintableString;

import static java.lang.System.lineSeparator;

public final class ShapelessResultPattern extends ShapelessPattern implements ResultPattern<ShapelessRecipe> {
    private static CharSequence representInput(List<Pair<Ingredient, Integer>> ingredientCounts) {
        return ingredientCounts.stream()
            .map(ingredientCount -> "%sx %s".formatted(
                ingredientCount.getSecond(),
                toPrintableString(ingredientCount.getFirst())
            ))
            .collect(Collectors.joining(lineSeparator()));
    }

    private static Stream<Optional<Ingredient>> streamIngredients(ShapelessRecipe recipe) {
        return ((ShapelessRecipeAccessor) recipe)
            .recipe_reshaper$getIngredients()
            .stream()
            .map(Optional::of);
    }

    private final String idSuffix;
    @Nullable
    private final String group;
    private final CraftingCategory category;

    public ShapelessResultPattern(
        ImmutableMultimap<Integer, Character> countPattern, Character outputToken, int outputCount,
        ImmutableMap<Character, Ingredient> commonIngredients, String idSuffix,
        @Nullable String group, CraftingCategory category
    ) {
        super(countPattern, outputToken, outputCount, commonIngredients);
        this.idSuffix = idSuffix;
        this.group = group;
        this.category = category;
    }

    @Override
    public String getIdSuffix() {
        return this.idSuffix;
    }

    @Override
    public Identifier getDescriptiveId(ShapelessRecipe recipe) {
        // shapeless_<ingredients>_to_<count>_<output>
        return Identifier.of(NAMESPACE, "shapeless_%s_%s".formatted(
            StringUtil.idPathOf(streamIngredients(recipe)),
            ResultPattern.toDescriptiveIdSuffix(
                ((ShapelessRecipeAccessor) recipe).recipe_reshaper$getResult()
            )
        ));
    }

    @Override
    public Optional<String> getGroup() {
        return Optional.ofNullable(this.group);
    }

    @Override
    public CraftingCategory getCategory() {
        return this.category;
    }

    @Override
    public int getOutputCount() {
        return this.outputCount;
    }

    @Override
    public Optional<RecipeHolder<ShapelessRecipe>> tryGenerateRecipe(
        IngredientMapping<?> ingredientMapping, Identifier reshaperId
    ) {
        final List<Pair<Ingredient, Integer>> ingredientCounts = new ArrayList<>();
        for (final Map.Entry<Integer, Character> entry : this.countPattern.entries()) {
            final int count = entry.getKey();
            final char token = entry.getValue();

            final Optional<Ingredient> resolvedIngredient =
                this.resolveIngredient(ingredientMapping, token)
                    // shapeless recipes can't have empty ingredients, flatten to single optional
                    .map(Emptyable::get)
                    .flatMap(Function.identity());

            if (resolvedIngredient.isPresent()) {
                ingredientCounts.add(new Pair<>(resolvedIngredient.get(), count));
            } else {
                return Optional.empty();
            }
        }

        final List<Ingredient> ingredients = ingredientCounts.stream()
                .flatMap(entry -> {
                    final Ingredient ingredient = entry.getFirst();
                    final int count = entry.getSecond();

                    return Stream.generate(() -> ingredient).limit(count);
                })
                .toList();

        return this.tryGenerateRecipe(
            ingredientMapping, reshaperId,
            (group, category, output) -> new ShapelessRecipe(group, category, output, ingredients),
            () -> representInput(ingredientCounts)
        );
    }

    public record Data(ShapelessPattern.Data shapeless, ResultPattern.Data result) implements ResultPattern.Assembler {
        public static final MapCodec<Data> CODEC = Codec.mapPair(
            ShapelessPattern.Data.codecOf(ResultPattern.Data.COMMON_CODEC),
            ResultPattern.Data.CODEC
        ).xmap(
            shapelessAndResult -> new Data(shapelessAndResult.getFirst(), shapelessAndResult.getSecond()),
            shapelessResult -> new Pair<>(shapelessResult.shapeless, shapelessResult.result)
        );

        @Override
        public DataResult<ShapelessResultPattern> assemble(
            Set<Character> tokenSet, ImmutableMap<Character, Ingredient> commonIngredients
        ) {
            return this.shapeless.createCountPattern(tokenSet, false)
                .map(flatPattern -> new ShapelessResultPattern(
                    flatPattern,
                    this.shapeless.common().outputToken(), this.shapeless.common().outputCount(),
                    commonIngredients,
                    this.result.idSuffix(), this.result.group().orElse(null), this.result.category()
                ));
        }
    }
}
