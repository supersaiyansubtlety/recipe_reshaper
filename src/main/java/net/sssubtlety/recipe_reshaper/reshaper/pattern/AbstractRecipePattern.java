package net.sssubtlety.recipe_reshaper.reshaper.pattern;

import com.google.common.collect.ImmutableMap;

import net.minecraft.recipe.Ingredient;

import net.sssubtlety.recipe_reshaper.util.CodecUtil;

import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import java.util.Optional;
import java.util.Set;

import static net.sssubtlety.recipe_reshaper.reshaper.Reshaper.EMPTY_TOKEN;

public abstract class AbstractRecipePattern implements RecipePattern {
    public final Character outputToken;
    public final int outputCount;
    public final ImmutableMap<Character, Ingredient> commonIngredients;

    public AbstractRecipePattern(
        Character outputToken, int outputCount, ImmutableMap<Character, Ingredient> commonIngredients
    ) {
        if (outputToken == EMPTY_TOKEN) {
            throw new IllegalArgumentException("Cannot use empty token ' ' (space) as output token");
        }

        if (outputCount < 1) {
            throw new IllegalArgumentException("Invalid output count: " + outputCount + ", must be at least 1");
        }

        this.outputToken = outputToken;
        this.outputCount = outputCount;
        this.commonIngredients = commonIngredients == null ? ImmutableMap.of() : commonIngredients;
    }

    @Override
    public Character getOutputToken() {
        return this.outputToken;
    }

    @Override
    public ImmutableMap<Character, Ingredient> getCommonIngredients() {
        return this.commonIngredients;
    }

    public record CommonData(char outputToken, Integer outputCount) {
        private static final MapCodec<Character> OUTPUT_TOKEN_CODEC =
            CodecUtil.TOKEN_CODEC.fieldOf(Keys.OUTPUT_TOKEN);

        public static MapCodec<CommonData> codecOf(Codec<Integer> outputCountBaseCodec) {
            return RecordCodecBuilder.mapCodec(
                instance -> instance.group(
                    OUTPUT_TOKEN_CODEC.forGetter(CommonData::outputToken),
                    outputCountBaseCodec.fieldOf(Keys.OUTPUT_COUNT).forGetter(CommonData::outputCount)
                ).apply(instance, CommonData::new)
            );
        }

        /**
         * @return an {@link Optional} containing an error message if an error was encountered,
         * or {@link Optional#empty()} otherwise
         */
        public static Optional<String> crossReferenceTokenSet(Set<Character> tokenSet, boolean expandSet, char token) {
            if (expandSet) {
                tokenSet.add(token);
            } else if (!tokenSet.contains(token)) {
                return Optional.of("Result pattern contains token not in source patterns.");
            }

            return Optional.empty();
        }
    }

    public interface Keys {
        String OUTPUT_TOKEN = "output_token";
        String OUTPUT_COUNT = "output_count";
        String PATTERN = "pattern";
    }
}
