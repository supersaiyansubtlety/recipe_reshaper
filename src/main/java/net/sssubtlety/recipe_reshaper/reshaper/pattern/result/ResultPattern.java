package net.sssubtlety.recipe_reshaper.reshaper.pattern.result;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.CraftingCategory;
import net.minecraft.recipe.CraftingRecipe;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.recipe.ShapedRecipe;
import net.minecraft.registry.Registries;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.util.Identifier;
import net.minecraft.util.dynamic.Codecs;

import net.sssubtlety.recipe_reshaper.reshaper.pattern.AbstractRecipePattern.CommonData;
import net.sssubtlety.recipe_reshaper.reshaper.pattern.RecipePattern;
import net.sssubtlety.recipe_reshaper.util.IngredientConvertible;
import net.sssubtlety.recipe_reshaper.util.Emptyable;
import net.sssubtlety.recipe_reshaper.reshaper.mapping.IngredientMapping;

import org.apache.commons.lang3.StringUtils;
import oshi.util.Memoizer;

import com.mojang.datafixers.util.Either;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

import static net.sssubtlety.recipe_reshaper.RecipeReshaper.LOGGER;
import static net.sssubtlety.recipe_reshaper.RecipeReshaper.NAMESPACE;
import static net.sssubtlety.recipe_reshaper.util.StringUtil.idPathOf;
import static net.sssubtlety.recipe_reshaper.util.IngredientUtil.streamItems;
import static net.sssubtlety.recipe_reshaper.util.StringUtil.tabIndent;

public sealed interface ResultPattern<R extends CraftingRecipe> extends RecipePattern
        permits ShapedResultPattern, ShapelessResultPattern {
    // heuristics:
    // - namespace is minecraft
    // - fewest "_"
    // - ends with "_block"
    // - starts with "block_of_"
    ImmutableList<ToIntFunction<Identifier>> HEURISTICS = ImmutableList.of(
        id -> id.getNamespace().equals("minecraft") ? 0 : 1,
        id -> StringUtils.countMatches(id.getPath(),"_"),
        id -> id.getPath().endsWith("_block") ? 0 : 1,
        id -> id.getPath().startsWith("block_of_") ? 0 : 1
    );

    String PRIMARY_ITEM_MESSAGE = "primary item for ingredient that became output";

    private static DataResult<Item> chooseOutputImpl(Ingredient ingredient) {
        final List<Item> matchingItems = streamItems(ingredient).toList();
        if (matchingItems.size() == 1) {
            return DataResult.success(matchingItems.getFirst());
        } else {
            // try to infer which item in matchingItems is the primary ingredient,
            // the one that should become the output
            ImmutableMap<Identifier, Item> candidates = matchingItems.stream()
                .distinct()
                .collect(ImmutableMap.toImmutableMap(Registries.ITEM::getId, Function.identity()));

            final List<ImmutableMap.Entry<Identifier, Item>> selectedCandidates = new LinkedList<>();

            for (final ToIntFunction<Identifier> heuristic : HEURISTICS) {
                int min = Integer.MAX_VALUE;
                for (final ImmutableMap.Entry<Identifier, Item> candidate : candidates.entrySet()) {
                    final int result = heuristic.applyAsInt(candidate.getKey());
                    if (result <= min) {
                        if (result < min) {
                            min = result;
                            selectedCandidates.clear();
                        }

                        selectedCandidates.add(candidate);
                    }
                }

                if (!selectedCandidates.isEmpty()) {
                    if (selectedCandidates.size() == 1) {
                        final Item outputItem = selectedCandidates.getFirst().getValue();

                        // TODO gate this behind global verbose config
                        LOGGER.info("Chose {} as {}", Registries.ITEM.getId(outputItem), PRIMARY_ITEM_MESSAGE);

                        return DataResult.success(outputItem);
                    }

                    candidates = ImmutableMap.copyOf(selectedCandidates);
                    selectedCandidates.clear();
                }
            }
        }

        return DataResult.error(() ->
            """
            could not infer %s; possibilities were:
            %s\
            """.formatted(
                PRIMARY_ITEM_MESSAGE,
                matchingItems.stream()
                    .map(Registries.ITEM::getId)
                    .toList()
            )
        );
    }

    /**
     * This returns {@code Optional<Emptyable<?>>} because {@link ShapedRecipe}s can take {@link Optional#empty()}
     * as an ingredient.
     * <ul>
     *     <li> {@code Optional.empty()} indicates an ingredient couldn't be resolved
     *     <li> {@code Optional.of(Emptyable.empty())} indicates an empty ingredient was resolved
     *     <li> {@code Optional.of(Emptyable.of(ingredient))} indicates a non-empty ingredient was resolved
     * </ul>
     */
    private static <I extends IngredientConvertible> Optional<Emptyable<Ingredient>> getIngredient(
        IngredientMapping<I> ingredientMapping,
        ImmutableMap<Character, Ingredient> commonIngredients, char key
    ) {
        final Ingredient commonIngredient = commonIngredients.get(key);
        if (commonIngredient == null) {
            final Optional<Emptyable<I>> mappedIngredient = ingredientMapping.get(key);
            if (mappedIngredient.isEmpty()) {
                return Optional.empty();
            } else {
                final Optional<I> emptyableIngredient = mappedIngredient.orElseThrow().get();
                if (emptyableIngredient.isEmpty()) {
                    return Optional.of(Emptyable.empty());
                } else {
                    final Optional<Ingredient> resolvedIngredient =
                        emptyableIngredient.orElseThrow().tryAsIngredient();

                    if (resolvedIngredient.isEmpty()) {
                        return Optional.empty();
                    } else {
                        return Optional.of(Emptyable.of(resolvedIngredient));
                    }
                }
            }
        } else {
            return Optional.of(Emptyable.of(commonIngredient));
        }
    }

    static String toDescriptiveIdSuffix(ItemStack output) {
        return "to_%s_%s".formatted(
            output.getCount(), idPathOf(Registries.ITEM.getId(output.getItem()))
        );
    }

    default Identifier reshapeId(Identifier oldId) {
        return Identifier.of(NAMESPACE, oldId.getNamespace() + "/" + oldId.getPath() + this.getIdSuffix());
    }

    String getIdSuffix();

    Optional<String> getGroup();

    CraftingCategory getCategory();

    int getOutputCount();

    Identifier getDescriptiveId(R recipe);

    /**
     * Tries to generate a recipe based on the passed {@code ingredientMapping}.
     * <p>
     * Implementations must {@linkplain #resolveIngredient resolve} each mapping of the passed {@code ingredientMapping}
     * and use {@link #tryGenerateRecipe(IngredientMapping, Identifier, RecipeFactory, Supplier)} with a
     * {@link RecipeFactory} that uses the {@linkplain #resolveIngredient resolved} ingredients.
     *
     * @param ingredientMapping an ingredient mapping of this pattern
     * @param reshaperId the id of the reshaper this pattern belongs to; used for error reporting
     * @return an {@link Optional} holding a {@link RecipeHolder} with the generated recipe if all ingredients of the
     * passed {@code ingredientMapping} could be {@linkplain #resolveIngredient resolved} and an output could be
     * {@linkplain #chooseOutput chosen}, or {@link Optional#empty()} otherwise
     */
    Optional<RecipeHolder<R>> tryGenerateRecipe(IngredientMapping<?> ingredientMapping, Identifier reshaperId);

    /**
     * Generates a recipe from the passed {@code ingredientMapping} using the passed {@code recipeFactory}
     * if an output can be {@linkplain #chooseOutput chosen}.
     *
     * @param ingredientMapping an ingredient mapping of this pattern whose ingredients have all been determined
     * @param reshaperId the id of the reshaper this pattern belongs to; used for error reporting
     * @param recipeFactory a method used to create a recipe if an output can be {@linkplain #chooseOutput chosen}
     * @param representInput a method used to create a readable representation of the generated recipe's input;
     *                       used for error reporting when an output can't be {@linkplain #chooseOutput chosen}
     *
     * @return an {@link Optional} holding a {@link RecipeHolder} with the recipe generated by the passed
     * {@code recipeFactory} if an output could be {@linkplain #chooseOutput chosen},
     * or {@link Optional#empty()} otherwise
     */
    default Optional<RecipeHolder<R>> tryGenerateRecipe(
        IngredientMapping<?> ingredientMapping, Identifier reshaperId,
        RecipeFactory<R> recipeFactory, Supplier<CharSequence> representInput
    ) {
        return this.chooseOutput(ingredientMapping, representInput, reshaperId)
            .map(outputItem -> {
                final ItemStack outputStack = new ItemStack(outputItem, this.getOutputCount());
                final Identifier outputId = Registries.ITEM.getId(outputItem);
                final Identifier recipeId = this.reshapeId(outputId);
                final String group = this.getGroup().orElse(outputId.getPath());

                return new RecipeHolder<>(
                    RegistryKey.of(RegistryKeys.RECIPE, recipeId),
                    recipeFactory.create(group, this.getCategory(), outputStack)
                );
            });
    }

    default Pair<Map<Identifier, RecipeHolder<?>>, List<Identifier>> generateRecipesAndRemovals(
        List<? extends IngredientMapping<?>> ingredientMappings, Identifier reshaperId
    ) {
        final Map<Identifier, RecipeHolder<?>> recipesById = new HashMap<>();

        final List<Identifier> idsToRemove = new LinkedList<>();
        for (final IngredientMapping<?> ingredientMapping : ingredientMappings) {
            this.tryGenerateRecipe(ingredientMapping, reshaperId).ifPresent(recipeHolder -> {
                final R recipe = recipeHolder.value();
                final Identifier id = recipeHolder.id().getValue();

                idsToRemove.addAll(ingredientMapping.streamSourcesToRemove().toList());

                final Predicate<Identifier> notDuplicate = ((Predicate<Identifier>) recipesById::containsKey).negate();
                Optional.of(id)
                    .filter(notDuplicate)
                    .or(() -> Optional.of(this.getDescriptiveId(recipe)))
                    .filter(notDuplicate)
                    // absent means a descriptiveId was a duplicate => an identical recipe was already generated
                    .ifPresent(uniqueId -> recipesById.put(
                        uniqueId,
                        new RecipeHolder<>(RegistryKey.of(RegistryKeys.RECIPE, uniqueId), recipe)
                    ));
            });
        }

        return new Pair<>(recipesById, idsToRemove);
    }

    default Optional<Emptyable<Ingredient>> resolveIngredient(IngredientMapping<?> ingredientMapping, char token) {
        return getIngredient(ingredientMapping, this.getCommonIngredients(), token);
    }

    /**
     * Determines the output item for the resulting recipe.
     * <p>
     * The returned item is present if both:
     * <ul>
     *     <li> an output ingredient mapping is present
     *     <li> an output item can be {@linkplain ResultPattern#chooseOutputImpl inferred} from the output ingredient
     * </ul>
     */
    private Optional<Item> chooseOutput(
        IngredientMapping<?> ingredientMapping, Supplier<CharSequence> representInput, Identifier reshaperId
    ) {
        final Supplier<CharSequence> representIndentedInput =
            Memoizer.memoize(() -> tabIndent(representInput.get()));

        return getIngredient(ingredientMapping, this.getCommonIngredients(), this.getOutputToken())
            .map(Emptyable::get)
            .flatMap(ingredient -> {
                if (ingredient.isEmpty()) {
                    LOGGER.error(
                        """
                        Reshaper "{}" generated empty output ingredient for input:
                        {}\
                        """,
                        reshaperId,
                        representIndentedInput.get()
                    );
                }

                return ingredient;
            })
            .map(ResultPattern::chooseOutputImpl)
            .flatMap(dataResult -> dataResult
                .ifError(error -> LOGGER.error(
                    """
                    Reshaper "{}" failed to generate recipe for input:
                    {}
                    {}\
                    """,
                    reshaperId,
                    representIndentedInput.get(),
                    tabIndent("Cause: " + error.message())
                ))
                .resultOrPartial()
            );
    }

    sealed interface Assembler permits ShapedResultPattern.Data, ShapelessResultPattern.Data {
        MapCodec<Assembler> CODEC = Codec
            .mapEither(ShapedResultPattern.Data.CODEC, ShapelessResultPattern.Data.CODEC)
            .xmap(Either::unwrap, assembler -> switch (assembler) {
                case ShapedResultPattern.Data shaped -> Either.left(shaped);
                case ShapelessResultPattern.Data shapeless -> Either.right(shapeless);
            });

        DataResult<? extends ResultPattern<?>> assemble(
            Set<Character> tokenSet, ImmutableMap<Character, Ingredient> commonIngredients
        );
    }

    record Data(String idSuffix, Optional<String> group, CraftingCategory category) {
        public static final CraftingCategory DEFAULT_CATEGORY = CraftingCategory.MISC;

        private static final Codec<String> ID_SUFFIX_CODEC = Codecs.NON_EMPTY_STRING.validate(string ->
            Identifier.isPathValid(string) ?
                DataResult.success(string) :
                DataResult.error(() -> "Non [a-z0-9/._-] character in " + Keys.ID_SUFFIX)
        );

        public static final MapCodec<Data> CODEC = RecordCodecBuilder.mapCodec(instance ->
            instance.group(
                ID_SUFFIX_CODEC.fieldOf(Keys.ID_SUFFIX).forGetter(Data::idSuffix),
                Codec.STRING.optionalFieldOf(Keys.GROUP).forGetter(Data::group),
                CraftingCategory.CODEC.optionalFieldOf(Keys.CATEGORY, DEFAULT_CATEGORY).forGetter(Data::category)
            ).apply(instance, Data::new)
        );

        private static final Codec<Integer> OUTPUT_COUNT_CODEC = Codecs.createRangedInt(1, 99);

        public static final MapCodec<CommonData> COMMON_CODEC = CommonData.codecOf(OUTPUT_COUNT_CODEC);

        public interface Keys {
            String ID_SUFFIX = "id_suffix";
            String GROUP = "group";
            String CATEGORY = "category";
        }
    }

    @FunctionalInterface
    interface RecipeFactory<R extends CraftingRecipe> {
        R create(String group, CraftingCategory category, ItemStack output);
    }
}
