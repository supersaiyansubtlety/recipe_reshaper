package net.sssubtlety.recipe_reshaper.reshaper.pattern.source;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.jetbrains.annotations.ApiStatus;

import net.minecraft.item.Item;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.registry.Holder;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;
import net.minecraft.util.StringIdentifiable;
import net.minecraft.util.dynamic.Codecs;

import net.sssubtlety.recipe_reshaper.reshaper.mapping.IngredientMapping;
import net.sssubtlety.recipe_reshaper.reshaper.pattern.AbstractRecipePattern.CommonData;
import net.sssubtlety.recipe_reshaper.reshaper.pattern.RecipePattern;
import net.sssubtlety.recipe_reshaper.util.CodecUtil;

import com.mojang.datafixers.util.Either;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static com.mojang.serialization.Codec.mapPair;

public sealed interface SourcePattern extends RecipePattern permits ShapedSourcePattern, ShapelessSourcePattern {
    // Ingredient::getItems
    @SuppressWarnings("deprecation")
    static boolean ingredientContainsSubstring(Ingredient ingredient, String substring) {
        for (final Holder<Item> item : ingredient.getItems().toList()) {
            if (Registries.ITEM.getId(item.getValue()).toString().contains(substring)) {
                return true;
            }
        }

        return false;
    }

    ImmutableMap<Character, Pair<String, Boolean>> getIdSubstrings();

    Optional<? extends IngredientMapping<?>> generateIngredientMapping(
        RecipeHolder<? extends Recipe<?>> recipe, ImmutableSet<Character> tokens
    );

    default boolean substringsAllow(char token, Ingredient ingredient) {
        final Pair<String, Boolean> stringAndNegate = this.getIdSubstrings().get(token);
        if (stringAndNegate != null) {
            final Boolean negate = stringAndNegate.getSecond();
            final String subsString = stringAndNegate.getFirst();
            // false iff: has substring but mustn't, or lacks substring but needs it
            return ingredientContainsSubstring(ingredient, subsString) != negate;
        }

        return true;
    }

    Stream<Identifier> streamIdsToAlwaysRemove();

    RemovalBehavior removalBehavior();

    sealed interface Assembler permits ShapedSourcePattern.Data, ShapelessSourcePattern.Data {
        MapCodec<Assembler> CODEC = Codec
            .mapEither(ShapedSourcePattern.Data.CODEC, ShapelessSourcePattern.Data.CODEC)
            .xmap(Either::unwrap, assembler -> switch (assembler) {
                case ShapedSourcePattern.Data shaped -> Either.left(shaped);
                case ShapelessSourcePattern.Data shapeless -> Either.right(shapeless);
            });

        DataResult<? extends SourcePattern> assemble(
            Set<Character> tokenSet, ImmutableMap<Character, Ingredient> commonIngredients,
            ImmutableMap<Character, Pair<String, Boolean>> idSubstringsByToken
        );
    }

    record Data(RemovalBehavior removalBehavior) {
        public static final MapCodec<Data> CODEC =
            RemovalBehavior.CODEC.xmap(Data::new, Data::removalBehavior);

        private static final Codec<Integer> OUTPUT_COUNT_CODEC = Codecs.createRangedInt(0, 99).orElse(0);

        public static final MapCodec<CommonData> COMMON_CODEC = CommonData.codecOf(OUTPUT_COUNT_CODEC);
    }

    enum RemovalBehavior implements StringIdentifiable {
        NEVER(new Pair<>(Optional.of(false), Optional.empty())),
        ALWAYS(new Pair<>(Optional.empty(), Optional.of(true))),
        NORMAL(new Pair<>(Optional.empty(), Optional.empty()));

        private static final MapCodec<RemovalBehavior> DIRECT_CODEC =
            StringIdentifiable.createEnumCodec(RemovalBehavior::values)
                .fieldOf(Keys.REMOVAL_BEHAVIOR);

        @Deprecated(forRemoval = true)
        @ApiStatus.ScheduledForRemoval(inVersion = "Minecraft 1.22")
        @SuppressWarnings("DeprecatedIsStillUsed")
        private static final MapCodec<RemovalBehavior> BOOL_COMBO_CODEC =
            mapPair(
                Codec.BOOL.optionalFieldOf(Keys.REMOVE),
                Codec.BOOL.optionalFieldOf(Keys.ALWAYS_REMOVE)
            ).flatXmap(
                removeAndAlwaysRemove -> {
                    final boolean remove = removeAndAlwaysRemove.getFirst().orElse(true);
                    final boolean alwaysRemove = removeAndAlwaysRemove.getSecond().orElse(false);

                    if (alwaysRemove) {
                        if (!remove) {
                            return DataResult.error(() ->
                                "\"%s\" (%s) contradicts \"%s\" (%s)"
                                    .formatted(Keys.ALWAYS_REMOVE, true, Keys.REMOVE, false)
                            );
                        }

                        return DataResult.success(ALWAYS);
                    } else if (remove) {
                        return DataResult.success(NORMAL);
                    } else {
                        return DataResult.success(NEVER);
                    }
                },
                CodecUtil.successOf(RemovalBehavior::getBoolRepresentation)
            );

        public static final MapCodec<RemovalBehavior> CODEC = CodecUtil.withAlternative(DIRECT_CODEC, BOOL_COMBO_CODEC);

        private final String name;
        private final Pair<Optional<Boolean>, Optional<Boolean>> boolRepresentation;

        RemovalBehavior(Pair<Optional<Boolean>, Optional<Boolean>> boolRepresentation) {
            this.boolRepresentation = boolRepresentation;
            this.name = this.name().toLowerCase();
        }

        @Override
        public String asString() {
            return this.name;
        }

        public Pair<Optional<Boolean>, Optional<Boolean>> getBoolRepresentation() {
            return this.boolRepresentation;
        }
    }

    interface Keys {
        String REMOVAL_BEHAVIOR = "removal_behavior";
        String REMOVE = "remove";
        String ALWAYS_REMOVE = "always_remove";
    }
}
