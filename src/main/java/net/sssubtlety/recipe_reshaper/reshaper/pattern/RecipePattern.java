package net.sssubtlety.recipe_reshaper.reshaper.pattern;

import com.google.common.collect.ImmutableMap;

import net.minecraft.recipe.Ingredient;

public interface RecipePattern {
    Character getOutputToken();

    ImmutableMap<Character, Ingredient> getCommonIngredients();
}
