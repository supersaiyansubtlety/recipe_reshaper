package net.sssubtlety.recipe_reshaper.reshaper.pattern.source;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.recipe.ShapelessRecipe;
import net.minecraft.util.Identifier;

import net.sssubtlety.recipe_reshaper.mixin.accessor.ShapelessRecipeAccessor;
import net.sssubtlety.recipe_reshaper.util.IngredientSet;
import net.sssubtlety.recipe_reshaper.reshaper.pattern.ShapelessPattern;
import net.sssubtlety.recipe_reshaper.reshaper.mapping.UnorderedMapping;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static net.sssubtlety.recipe_reshaper.util.IngredientUtil.areEquivalent;

public final class ShapelessSourcePattern extends ShapelessPattern implements SourcePattern {
    private final Multiset<Integer> countOccurrences;
    private final ImmutableMap<Character, Pair<String, Boolean>> idSubstrings;
    private final RemovalBehavior removalBehavior;
    private final List<Identifier> idsToAlwaysRemove;

    private ShapelessSourcePattern(
        ImmutableMultimap<Integer, Character> countPattern, Character outputToken, int outputCount,
        RemovalBehavior removalBehavior, ImmutableMap<Character, Ingredient> commonIngredients,
        ImmutableMap<Character, Pair<String, Boolean>> idSubstrings
    ) {
        super(countPattern, outputToken, outputCount, commonIngredients);
        this.countOccurrences = this.countOccurrences(countPattern.keys());
        this.idSubstrings = idSubstrings;
        this.removalBehavior = removalBehavior;
        this.idsToAlwaysRemove = new LinkedList<>();
    }

    private Multiset<Integer> countOccurrences(Collection<Integer> integers) {
        final Multiset<Integer> countOccurrences = HashMultiset.create();
        for (final Integer integer : integers) {
            countOccurrences.add(integer, 1);
        }

        return countOccurrences;
    }

    @Override
    public ImmutableMap<Character, Pair<String, Boolean>> getIdSubstrings() {
        return this.idSubstrings;
    }

    @Override
    public Optional<UnorderedMapping> generateIngredientMapping(
        RecipeHolder<? extends Recipe<?>> recipe, ImmutableSet<Character> tokens
    ) {
        if (!(recipe.value() instanceof ShapelessRecipe shapelessRecipe)) {
            return Optional.empty();
        }

        if (
            this.outputCount > 0 &&
                ((ShapelessRecipeAccessor) shapelessRecipe).recipe_reshaper$getResult().getCount() != this.outputCount
        ) {
            return Optional.empty();
        }

        final Multiset<Ingredient> ingredientCounts;
        {
            final List<Ingredient> ingredients =
                ((ShapelessRecipeAccessor) shapelessRecipe).recipe_reshaper$getIngredients();
            if (this.countOccurrences.size() != ingredients.size()) {
                return Optional.empty();
            }

            ingredientCounts = HashMultiset.create(ingredients);
        }

        final Multiset<Integer> ingredientCountOccurrences = this.countOccurrences(
            ingredientCounts.entrySet().stream()
                .map(Multiset.Entry::getCount)
                .toList()
        );

        if (!ingredientCountOccurrences.equals(this.countOccurrences)) {
            return Optional.empty();
        }

        final Multimap<Integer, Ingredient> ingredientsByCount = HashMultimap.create();
        for (final Multiset.Entry<Ingredient> entry : ingredientCounts.entrySet()) {
            ingredientsByCount.put(entry.getCount(), entry.getElement());
        }

        final Identifier recipeId = recipe.id().getValue();
        final UnorderedMapping unorderedMapping = this.removalBehavior() == RemovalBehavior.NORMAL ?
            new UnorderedMapping(tokens, recipeId) :
            new UnorderedMapping(tokens);

        for (final Integer count : this.countPattern.keySet()) {
            final ImmutableCollection<Character> tokensWithCount = this.countPattern.get(count);
            final Collection<Ingredient> ingredients = ingredientsByCount.get(count);

            for (final Character token : tokensWithCount) {
                if (this.commonIngredients.containsKey(token)) {
                    final Ingredient commonIngredient = this.commonIngredients.get(token);
                    if (ingredients.stream().noneMatch(ingredient -> areEquivalent(ingredient, commonIngredient))) {
                        return Optional.empty();
                    }
                }

                if (!unorderedMapping.put(token, new IngredientSet(ingredients))) {
                    return Optional.empty();
                }
            }
        }

        // handle result separately
        if (
            !unorderedMapping.put(
                this.outputToken,
                new IngredientSet(Ingredient.ofItem(
                    ((ShapelessRecipeAccessor) shapelessRecipe).recipe_reshaper$getResult().getItem()
                ))
            )
        ) {
            return Optional.empty();
        }

        if (!unorderedMapping.reduce(this.getIdSubstrings())) {
            return Optional.empty();
        }

        if (this.removalBehavior() == RemovalBehavior.ALWAYS) {
            this.idsToAlwaysRemove.add(recipeId);
        }

        return Optional.of(unorderedMapping);
    }

    @Override
    public Stream<Identifier> streamIdsToAlwaysRemove() {
        return this.idsToAlwaysRemove.stream();
    }

    @Override
    public RemovalBehavior removalBehavior() {
        return this.removalBehavior;
    }

    public record Data(ShapelessPattern.Data shapeless, SourcePattern.Data source) implements SourcePattern.Assembler {
        public static final MapCodec<Data> CODEC = Codec.mapPair(
            ShapelessPattern.Data.codecOf(SourcePattern.Data.COMMON_CODEC),
            SourcePattern.Data.CODEC
        ).xmap(
            shapelessAndSource -> new Data(shapelessAndSource.getFirst(), shapelessAndSource.getSecond()),
            shapelessSource -> new Pair<>(shapelessSource.shapeless, shapelessSource.source)
        );

        @Override
        public DataResult<ShapelessSourcePattern> assemble(
            Set<Character> tokenSet, ImmutableMap<Character, Ingredient> commonIngredients,
            ImmutableMap<Character, Pair<String, Boolean>> idSubstringsByToken
        ) {
            return this.shapeless.createCountPattern(tokenSet, true)
                .map(countPattern -> new ShapelessSourcePattern(
                    countPattern,
                    this.shapeless.common().outputToken(), this.shapeless.common().outputCount(),
                    this.source.removalBehavior(),
                    commonIngredients, idSubstringsByToken
                ));
        }
    }
}
