package net.sssubtlety.recipe_reshaper.reshaper.pattern.source;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.recipe.ShapedRecipe;
import net.minecraft.util.Identifier;

import net.sssubtlety.recipe_reshaper.mixin.accessor.ShapedRecipeAccessor;
import net.sssubtlety.recipe_reshaper.reshaper.pattern.ShapedPattern;
import net.sssubtlety.recipe_reshaper.reshaper.mapping.OrderedMapping;
import net.sssubtlety.recipe_reshaper.util.IngredientWrapper;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static net.sssubtlety.recipe_reshaper.reshaper.Reshaper.EMPTY_TOKEN;
import static net.sssubtlety.recipe_reshaper.util.IngredientUtil.areEquivalent;

public final class ShapedSourcePattern extends ShapedPattern implements SourcePattern {
    private final RemovalBehavior removalBehavior;
    private final List<Identifier> idsToAlwaysRemove;
    private final ImmutableMap<Character, Pair<String, Boolean>> idSubstrings;

    private ShapedSourcePattern(
        Character[] pattern, Character outputToken, int outputCount, int width, int height,
        RemovalBehavior removalBehavior, ImmutableMap<Character, Ingredient> commonIngredients,
        ImmutableMap<Character, Pair<String, Boolean>> idSubstrings
    ) {
        super(pattern, outputToken, outputCount, width, height, commonIngredients);

        this.removalBehavior = removalBehavior;
        this.idSubstrings = idSubstrings;
        this.idsToAlwaysRemove = new LinkedList<>();
    }

    @Override
    public ImmutableMap<Character, Pair<String, Boolean>> getIdSubstrings() {
        return this.idSubstrings;
    }

    @Override
    public Optional<OrderedMapping> generateIngredientMapping(
        RecipeHolder<? extends Recipe<?>> recipe, ImmutableSet<Character> tokens
    ) {
        if (!(recipe.value() instanceof final ShapedRecipe shapedRecipe)) {
            return Optional.empty();
        }

        final ItemStack result = ((ShapedRecipeAccessor) shapedRecipe).recipe_reshaper$getResult();

        if (this.outputCount > 0 && result.getCount() != this.outputCount) {
            return Optional.empty();
        }

        if (shapedRecipe.getWidth() != this.width || shapedRecipe.getHeight() != this.height) {
            return Optional.empty();
        }

        final Identifier recipeId = recipe.id().getValue();
        final OrderedMapping ingredientMapping = this.removalBehavior() == RemovalBehavior.NORMAL ?
            new OrderedMapping(tokens, recipeId)
            : new OrderedMapping(tokens);

        final List<Optional<Ingredient>> ingredients = shapedRecipe.getIngredients();

        for (int i = 0; i < this.pattern.length; i++) {
            final Optional<Ingredient> optIngredient = ingredients.get(i);
            final char token = this.pattern[i];

            final boolean emptyIngredient = optIngredient.isEmpty();
            final boolean emptyToken = token == EMPTY_TOKEN;
            if (emptyIngredient == emptyToken) {
                if (emptyIngredient) {
                    continue;
                }

                final Ingredient ingredient = optIngredient.orElseThrow();
                if (this.commonIngredients.containsKey(token)) {
                    if (!areEquivalent(ingredient, this.commonIngredients.get(token))) {
                        return Optional.empty();
                    }
                } else {
                    if (!this.substringsAllow(token, ingredient)) {
                        return Optional.empty();
                    }

                    if (!ingredientMapping.put(token, new IngredientWrapper(ingredient))) {
                        // incorrect mapping
                        return Optional.empty();
                    }
                    // else correct mapping
                }
            } else {
                return Optional.empty();
            }
        }

        // handle result separately
        if (!ingredientMapping.put(this.outputToken, new IngredientWrapper(Ingredient.ofItem(result.getItem())))) {
            // incorrect mapping
            return Optional.empty();
        }

        if (this.removalBehavior() == RemovalBehavior.ALWAYS) {
            this.idsToAlwaysRemove.add(recipeId);
        }

        return Optional.of(ingredientMapping);
    }

    @Override
    public Stream<Identifier> streamIdsToAlwaysRemove() {
        return this.idsToAlwaysRemove.stream();
    }

    @Override
    public RemovalBehavior removalBehavior() {
        return this.removalBehavior;
    }

    public record Data(ShapedPattern.Data shaped, SourcePattern.Data source) implements SourcePattern.Assembler {
        public static final MapCodec<Data> CODEC = Codec.mapPair(
            ShapedPattern.Data.codecOf(SourcePattern.Data.COMMON_CODEC),
            SourcePattern.Data.CODEC
        ).xmap(
            shapedAndSource -> new Data(shapedAndSource.getFirst(), shapedAndSource.getSecond()),
            shapedSource -> new Pair<>(shapedSource.shaped, shapedSource.source)
        );

        @Override
        public DataResult<SourcePattern> assemble(
            Set<Character> tokenSet, ImmutableMap<Character, Ingredient> commonIngredients,
            ImmutableMap<Character, Pair<String, Boolean>> idSubstringsByToken
        ) {
            return this.shaped.createFlatPattern(tokenSet, true)
                .map(flatPattern -> new ShapedSourcePattern(
                    flatPattern,
                    this.shaped.common().outputToken(), this.shaped.common().outputCount(),
                    this.shaped.width(), this.shaped.height(),
                    this.source.removalBehavior(),
                    commonIngredients, idSubstringsByToken
                ));
        }
    }
}
