package net.sssubtlety.recipe_reshaper.reshaper;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import org.jetbrains.annotations.Nullable;

import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.recipe.RecipeType;
import net.minecraft.registry.HolderLookup;
import net.minecraft.registry.ResourceFileNamespace;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.dynamic.Codecs;
import net.minecraft.util.profiler.Profiler;

import net.sssubtlety.recipe_reshaper.reshaper.mapping.IngredientMapping;
import net.sssubtlety.recipe_reshaper.util.CodecUtil;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.JsonOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static net.sssubtlety.recipe_reshaper.RecipeReshaper.LOGGER;
import static net.sssubtlety.recipe_reshaper.RecipeReshaper.NAMESPACE;
import static net.sssubtlety.recipe_reshaper.util.StringUtil.tabIndent;

/*
    TODO:
     - datagen
     - gametest
     - warn when generating multiple recipes with same id, append suffix and create anyways
     - LOGGER.warn() when no recipes are generated
     - check for conflicts and warn
     - add `/recipe_reshaper export[ <reshapers...>]` command to 'bake' generated
       recipes from a reshaper into static, vanilla recipes
     - add `/recipe_reshaper log[ <reshaper...>]` command to print or export:
        - inferred ingredients
        - inference failures
        - detected recipe conflicts
        - number of generated recipes
        - generated recipe ids
        - which recipe ids contributed to which generated recipes
     - add optional require_all parameter to data packs to require all
       source patterns be matched for a recipe to be output
     - add id_regex (check namespace and path)
 */
public final class Reshaper {
    public static final char EMPTY_TOKEN = ' ';
    public static final String EMPTY_TOKEN_STRING = String.valueOf(EMPTY_TOKEN);

    // used for error reporting
    public final Identifier id;

    private final SourceFamily sourceFamily;

    @Nullable
    private final ResultFamily resultFamily;

    private Reshaper(Identifier id, SourceFamily sourceFamily, @Nullable ResultFamily resultFamily) {
        this.id = id;
        this.sourceFamily = sourceFamily;
        this.resultFamily = resultFamily;
    }

    void check(RecipeHolder<? extends Recipe<?>> recipe) {
        this.sourceFamily.check(recipe);
    }

    public Pair<Map<Identifier, RecipeHolder<?>>, List<Identifier>> generateRecipesAndRemovals() {
        final List<IngredientMapping<?>> ingredientMaps = this.sourceFamily.getMergedMappings();
        final Map<Identifier, RecipeHolder<?>> recipesById = new HashMap<>();
        final List<Identifier> idsToRemove = this.sourceFamily.streamIdsToAlwaysRemove()
            .collect(Collectors.toCollection(LinkedList::new));
        // resultFamily is optional
        if (this.resultFamily != null) {
            final Pair<Map<Identifier, RecipeHolder<?>>, List<Identifier>> recipesAndRemovals =
                this.resultFamily.generateRecipesAndRemovals(ingredientMaps, this.id);

            recipesById.putAll(recipesAndRemovals.getFirst());
            idsToRemove.addAll(recipesAndRemovals.getSecond());
        }

        this.sourceFamily.clearMappings();

        return new Pair<>(recipesById, idsToRemove);
    }

    public RecipeType<?> getType() {
        return RecipeType.CRAFTING;
    }

    public record Data(
        ImmutableMap<Character, Ingredient> commonIngredients,
        ImmutableMap<Character, String> idSubstrings,
        SourceFamily.Data sourceFamilyData,
        Optional<ResultFamily.Data> resultFamilyData
    ) {
        private static final Codec<ImmutableMap<Character, Ingredient>> COMMON_INGREDIENTS_CODEC = Codecs
            .createStrictUnboundedMap(CodecUtil.TOKEN_CODEC, Ingredient.ALLOW_EMPTY_CODEC)
            .xmap(ImmutableMap::copyOf, Function.identity());

        private static final Codec<ImmutableMap<Character, String>> ID_SUBSTRINGS_CODEC = Codecs
            .createStrictUnboundedMap(CodecUtil.TOKEN_CODEC, Codecs.NON_EMPTY_STRING)
            .xmap(ImmutableMap::copyOf, Function.identity());

        public static final Codec<Data> CODEC = RecordCodecBuilder.<Data>mapCodec(instance ->
            instance.group(
                COMMON_INGREDIENTS_CODEC
                    .optionalFieldOf(Keys.COMMON_INGREDIENTS, ImmutableMap.of())
                    .forGetter(Data::commonIngredients),
                ID_SUBSTRINGS_CODEC
                    .optionalFieldOf(Keys.ID_SUBSTRINGS, ImmutableMap.of())
                    .forGetter(Data::idSubstrings),
                SourceFamily.Data.CODEC
                    .fieldOf(Keys.SOURCE_FAMILY)
                    .forGetter(Data::sourceFamilyData),
                ResultFamily.Data.CODEC
                    .optionalFieldOf(Keys.RESULT_FAMILY)
                    .forGetter(Data::resultFamilyData)
            ).apply(instance, Data::new)
        ).codec();

        public DataResult<Reshaper> assemble(Identifier id) {
            return this.sourceFamilyData.assemble(this.commonIngredients, this.idSubstrings)
                .flatMap(sourceFamily -> this.resultFamilyData
                    .map(data -> data
                        .assemble(sourceFamily.tokens, this.commonIngredients)
                        .map(resultFamily -> new Reshaper(id, sourceFamily, resultFamily))
                    )
                    .orElseGet(() -> DataResult.success(new Reshaper(id, sourceFamily, null)))
                );
        }

        public static final class Manager extends JsonDataLoader<Reshaper.Data> {
            public static final String TYPE = NAMESPACE + "s";

            private ImmutableMultimap<RecipeType<?>, Reshaper> reshapersByType;

            public Manager(HolderLookup.Provider provider, Codec<Reshaper.Data> codec, String dataType) {
                // We need the provider-derived ops because we need access to the registry so we can deserialize
                // ingredients in common_ingredients.
                // This uses the private (access-widened) constructor, not the one that takes a RegistryKey, because
                // net.fabricmc.fabric.mixin.registry.sync.RegistryKeysMixin::prependDirectoryWithNamespace
                // makes the resource path <namespace>/<path> instead of just <path>.
                super(
                    provider.createSerializationContext(JsonOps.INSTANCE),
                    codec,
                    ResourceFileNamespace.json(dataType)
                );
            }

            @Override
            protected void apply(
                Map<Identifier, Reshaper.Data> cache, ResourceManager manager, Profiler profiler
            ) {
                final ImmutableMultimap.Builder<RecipeType<?>, Reshaper> reshapersByTypeBuilder =
                    ImmutableMultimap.builder();

                cache.entrySet().stream()
                    .flatMap(entry -> {
                        final Identifier id = entry.getKey();
                        return entry.getValue().assemble(id)
                            .ifError(error -> LOGGER.error(
                                """
                                Error(s) in reshaper "{}":
                                {}\
                                """,
                                id,
                                tabIndent(error.message())
                            ))
                            .resultOrPartial()
                            .stream();
                    })
                    .forEach(reshaper -> reshapersByTypeBuilder.put(reshaper.getType(), reshaper));

                this.reshapersByType = reshapersByTypeBuilder.build();
            }

            public void checkRecipe(RecipeHolder<? extends Recipe<?>> recipe) {
                this.reshapersByType.get(recipe.value().getType()).forEach(reshaper ->
                    reshaper.check(recipe)
                );
            }

            public Iterable<RecipeHolder<?>> generateAndRemoveRecipes(
                RecipeType<?> type, Collection<RecipeHolder<?>> recipes
            ) {
                final Map<Identifier, RecipeHolder<?>> recipesById = recipes.stream()
                    .collect(Collectors.toMap(
                        recipe -> recipe.id().getValue(),
                        Function.identity(),
                        (left, right) -> {
                            throw new IllegalStateException(
                                """
                                Recipe holders have same id:
                                \t%s
                                \t%s\
                                """.formatted(left, right)
                            );
                        },
                        HashMap::new
                    ));

                final List<Identifier> idsToRemove = new LinkedList<>();

                this.reshapersByType.get(type).stream()
                    .map(Reshaper::generateRecipesAndRemovals)
                    .forEach(recipeMapAndIds2Remove -> {
                        recipesById.putAll(recipeMapAndIds2Remove.getFirst());
                        idsToRemove.addAll(recipeMapAndIds2Remove.getSecond());
                    });

                // delay removal until all reshapers have generated recipes so
                //   all source reshapers have access to all the original recipes
                idsToRemove.forEach(recipesById::remove);
                return recipesById.values();
            }
        }
    }

    public interface Keys {
        String ID_SUBSTRINGS = "id_substrings";
        String COMMON_INGREDIENTS = "common_ingredients";
        String SOURCE_FAMILY = "source_family";
        String RESULT_FAMILY = "result_family";
    }
}
