package net.sssubtlety.recipe_reshaper.reshaper;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.util.Identifier;

import net.sssubtlety.recipe_reshaper.reshaper.pattern.source.SourcePattern;
import net.sssubtlety.recipe_reshaper.reshaper.mapping.IngredientMapping;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import static net.sssubtlety.recipe_reshaper.util.StringUtil.lineJoin;

public final class SourceFamily {
    public final ImmutableList<SourcePattern> sourcePatterns;

    public final ImmutableSet<Character> tokens;

    private List<IngredientMapping<?>> ingredientMappings = new LinkedList<>();

    private SourceFamily(ImmutableList<SourcePattern> sourcePatterns, ImmutableSet<Character> tokens) {
        this.sourcePatterns = sourcePatterns;
        this.tokens = tokens;
    }

    private static ImmutableMap<Character, Pair<String, Boolean>> makeSubstringMapWithNegations(
        Map<Character, String> tokenIdSubstringMap
    ) {
        final ImmutableMap.Builder<Character, Pair<String, Boolean>> mapBuilder = ImmutableMap.builder();
        tokenIdSubstringMap.forEach((key, string) -> {
            final boolean negated = string.startsWith("!");
            if (negated) {
                string = string.substring(1);
            }

            mapBuilder.put(key, new Pair<>(string, negated));
        });

        return mapBuilder.build();
    }

    public void check(RecipeHolder<? extends Recipe<?>> recipe) {
        for (final SourcePattern pattern : this.sourcePatterns) {
            pattern.generateIngredientMapping(recipe, this.tokens)
                .ifPresent(this.ingredientMappings::add);
        }
    }

    public List<IngredientMapping<?>> getMergedMappings() {
        this.ingredientMappings = IngredientMapping.mergeMappings(this.ingredientMappings);
        return this.ingredientMappings;
    }

    public Stream<Identifier> streamIdsToAlwaysRemove() {
        return this.sourcePatterns.stream()
            .flatMap(SourcePattern::streamIdsToAlwaysRemove);
    }

    void clearMappings() {
        this.ingredientMappings.clear();
    }

    public record Data(ImmutableList<SourcePattern.Assembler> assemblers) {
        public static final Codec<Data> CODEC = SourcePattern.Assembler.CODEC.codec().listOf()
            .xmap(ImmutableList::copyOf, Function.identity())
            .xmap(Data::new, Data::assemblers);

        /**
         * Creates a {@link SourceFamily} with the combined tokens of all
         * {@linkplain #assemblers() assembled} {@link SourcePattern}s.
         */
        public DataResult<SourceFamily> assemble(
            ImmutableMap<Character, Ingredient> commonIngredients, ImmutableMap<Character, String> idSubstringMap
        ) {
            final ImmutableList.Builder<SourcePattern> patterns = ImmutableList.builder();
            final Set<Character> tokens = new HashSet<>();
            final List<String> errors = new LinkedList<>();
            for (final SourcePattern.Assembler assembler : this.assemblers) {
                assembler
                    .assemble(
                        tokens, commonIngredients, idSubstringMap == null ?
                            ImmutableMap.of() :
                            makeSubstringMapWithNegations(idSubstringMap)
                    )
                    .ifError(error -> errors.add(error.message()))
                    .resultOrPartial()
                    .ifPresent(patterns::add);
            }

            if (errors.isEmpty()) {
                return DataResult.success(new SourceFamily(patterns.build(), ImmutableSet.copyOf(tokens)));
            } else {
                return DataResult.error(() -> lineJoin(errors));
            }
        }
    }
}
