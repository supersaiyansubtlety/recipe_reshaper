package net.sssubtlety.recipe_reshaper.reshaper.mapping;

import net.sssubtlety.recipe_reshaper.util.IngredientWrapper;

import com.google.common.collect.ImmutableSet;

import net.minecraft.util.Identifier;

import java.util.Optional;

import static net.sssubtlety.recipe_reshaper.util.IngredientUtil.areEquivalent;

public final class OrderedMapping extends IngredientMapping<IngredientWrapper> {
    public OrderedMapping(ImmutableSet<Character> tokens) {
        super(tokens);
    }

    public OrderedMapping(ImmutableSet<Character> tokens, Identifier sourceId) {
        super(tokens, sourceId);
    }

    private OrderedMapping(OrderedMapping orderedMapping) {
        super(orderedMapping);
    }

    @Override
    protected Optional<? extends IngredientMapping<?>> tryMerge(IngredientMapping<?> other) {
        return other instanceof OrderedMapping otherOrdered ?
            this.tryMergeImpl(otherOrdered) :
            other.tryMerge(this);
    }

    @Override
    protected Optional<IngredientWrapper> getImpl(char token) {
        return this.ingredientPossibilities.get(token);
    }

    @Override
    protected OrderedMapping copy() {
        return new OrderedMapping(this);
    }

    @Override
    protected Optional<IngredientWrapper> mergeIngredients(IngredientWrapper ours, IngredientWrapper theirs) {
        return areEquivalent(ours.asIngredient(), theirs.asIngredient()) ?
            Optional.of(ours) :
            Optional.empty();
    }
}
