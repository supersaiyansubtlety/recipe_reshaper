package net.sssubtlety.recipe_reshaper.reshaper.mapping;

import net.sssubtlety.recipe_reshaper.util.IngredientConvertible;
import net.sssubtlety.recipe_reshaper.util.Emptyable;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import net.minecraft.util.Identifier;
import org.apache.commons.lang3.mutable.MutableObject;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import static net.sssubtlety.recipe_reshaper.reshaper.Reshaper.EMPTY_TOKEN;

public abstract sealed class IngredientMapping<I extends IngredientConvertible>
        permits OrderedMapping, UnorderedMapping {
    protected final TokenMap<I> ingredientPossibilities;

    protected final Set<Identifier> sourcesToRemove;

    public IngredientMapping(ImmutableSet<Character> tokens) {
        if (tokens.contains(EMPTY_TOKEN)) {
            throw new IllegalArgumentException("IngredientMapping received empty token ' ' (space)");
        }

        this.ingredientPossibilities = new TokenMap<>(tokens);
        this.sourcesToRemove = new LinkedHashSet<>();
    }

    public IngredientMapping(ImmutableSet<Character> tokens, Collection<Identifier> sourcesToRemove) {
        this(tokens);
        this.sourcesToRemove.addAll(sourcesToRemove);
    }

    public IngredientMapping(ImmutableSet<Character> tokens, Identifier sourceToRemove) {
        this(tokens);
        this.sourcesToRemove.add(sourceToRemove);
    }

    public IngredientMapping(IngredientMapping<I> ingredientMapping) {
        this.ingredientPossibilities = TokenMap.copyOf(ingredientMapping.ingredientPossibilities);
        this.sourcesToRemove = new LinkedHashSet<>(ingredientMapping.sourcesToRemove);
    }

    public Stream<Identifier> streamSourcesToRemove() {
        return this.sourcesToRemove.stream();
    }

    public static List<IngredientMapping<?>> mergeMappings(List<IngredientMapping<?>> mappings) {
        final List<IngredientMapping<?>> mergedMappings = new LinkedList<>();
        final int mappingsSize = mappings.size();

        for (int i = 0; i < mappingsSize; i++) {
            IngredientMapping<?> iMapping = mappings.get(i);
            if (iMapping == null) {
                continue;
            }

            for (int j = i + 1; j < mappingsSize; j++) {
                final IngredientMapping<?> jMapping = mappings.get(j);
                if (jMapping == null) {
                    continue;
                }

                final Optional<? extends IngredientMapping<?>> mergedMapping = iMapping.tryMerge(jMapping);
                if (mergedMapping.isPresent()) {
                    iMapping = mergedMapping.get();
                    mappings.set(j, null);
                }
            }

            mergedMappings.add(iMapping);
        }

        return mergedMappings;
    }

    protected abstract Optional<? extends IngredientMapping<?>> tryMerge(IngredientMapping<?> other);

    public final Optional<Emptyable<I>> get(char token) {
        if (token == EMPTY_TOKEN) {
            return Optional.of(Emptyable.empty());
        }

        final Optional<I> ingredient = this.getImpl(token);
        if (ingredient.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(Emptyable.of(ingredient));
        }
    }

    protected abstract Optional<I> getImpl(char token);

    protected abstract IngredientMapping<I> copy();

    protected final Optional<IngredientMapping<I>> tryMergeImpl(IngredientMapping<I> other) {
        final IngredientMapping<I> thisCopy = this.copy();
        for (final Character token : this.ingredientPossibilities.getTokens()) {
            final Optional<I> ours = thisCopy.ingredientPossibilities.get(token);
            final boolean thisMapped = ours.isPresent();
            final Optional<I> theirs = other.ingredientPossibilities.get(token);
            final boolean otherMapped = theirs.isPresent();

            if (thisMapped && otherMapped) {
                // both mapped: try merge
                final Optional<I> merged = this.mergeIngredients(ours.orElseThrow(), theirs.orElseThrow());
                if (merged.isPresent()) {
                    // can merge: merge
                    thisCopy.put(token, merged.get(), other.sourcesToRemove);
                } else {
                    // can't merge: fail
                    return Optional.empty();
                }
            } else if (otherMapped) {
                // only other is mapped: merge (copy) it
                thisCopy.put(token, theirs.orElseThrow(), other.sourcesToRemove);
            }
            // else either only this or neither is mapped: no merging
        }

        return Optional.of(thisCopy);
    }

    private void put(char token, I ingredient, Collection<Identifier> sourcesToRemove) {
        this.ingredientPossibilities.put(token, ingredient);
        this.sourcesToRemove.addAll(sourcesToRemove);
    }

    protected abstract Optional<I> mergeIngredients(I ours, I theirs);

    public final boolean put(Character token, I ingredient) {
        final Optional<I> current = this.ingredientPossibilities.get(token);
        if (current.isPresent()) {
            final Optional<I> merged = this.mergeIngredients(current.orElseThrow(), ingredient);
            if (merged.isPresent()) {
                this.ingredientPossibilities.put(token, merged.orElseThrow());

                return true;
            } else {
                return false;
            }
        } else {
            // not yet mapped
            this.ingredientPossibilities.put(token, ingredient);

            return true;
        }
    }

    protected static final class TokenMap<I> {
        // a map with fixed keys but mutable values
        private final ImmutableMap<Character, MutableObject<I>> delegate;

        public static <I> TokenMap<I> copyOf(TokenMap<I> map) {
            return new TokenMap<>(
                map.delegate.entrySet().stream().collect(
                    ImmutableMap.toImmutableMap(
                        Map.Entry::getKey,
                        // 'deep' copy mutable objects
                        entry -> new MutableObject<>(entry.getValue().getValue())
                    )
                )
            );
        }

        public TokenMap(ImmutableSet<Character> tokens) {
            // initialize map so that each key is associated with an empty mutable object
            this(tokens.stream().collect(ImmutableMap.toImmutableMap(
                Function.identity(),
                unused -> new MutableObject<>()
            )));
        }

        private TokenMap(ImmutableMap<Character, MutableObject<I>> delegate) {
            this.delegate = delegate;
        }

        public ImmutableSet<Character> getTokens() {
            return this.delegate.keySet();
        }

        public Optional<I> get(char token) {
            return Optional.ofNullable(this.getMutable(token).getValue());
        }

        public void put(char token, I i) {
            final MutableObject<I> value = this.getMutable(token);
            value.setValue(i);
        }

        private MutableObject<I> getMutable(char token) {
            final MutableObject<I> value = this.delegate.get(token);
            if (value == null) {
                throw new IllegalArgumentException("Received unrecognized token: " + token);
            }

            return value;
        }
    }
}
