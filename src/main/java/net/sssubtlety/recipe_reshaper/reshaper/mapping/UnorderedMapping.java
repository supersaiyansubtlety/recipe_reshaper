package net.sssubtlety.recipe_reshaper.reshaper.mapping;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import net.minecraft.util.Identifier;
import net.sssubtlety.recipe_reshaper.util.IngredientSet;
import net.sssubtlety.recipe_reshaper.util.IngredientWrapper;
import net.sssubtlety.recipe_reshaper.reshaper.pattern.source.SourcePattern;

import com.mojang.datafixers.util.Pair;

import java.util.LinkedList;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;

public final class UnorderedMapping extends IngredientMapping<IngredientSet> {
    public UnorderedMapping(ImmutableSet<Character> tokens) {
        super(tokens);
    }

    public UnorderedMapping(ImmutableSet<Character> tokens, Identifier sourceId) {
        super(tokens, sourceId);
    }

    public UnorderedMapping(ImmutableSet<Character> tokens, Set<Identifier> sourcesToRemove) {
        super(tokens, sourcesToRemove);
    }

    private UnorderedMapping(OrderedMapping orderedMapping) {
        this(orderedMapping.ingredientPossibilities.getTokens(), orderedMapping.sourcesToRemove);

        for (final Character token : orderedMapping.ingredientPossibilities.getTokens()) {
            final Optional<IngredientWrapper> ingredient = orderedMapping.ingredientPossibilities.get(token);
            if (ingredient.isPresent()) {
                this.ingredientPossibilities.put(token, new IngredientSet(ingredient.orElseThrow().asIngredient()));
            }
        }
    }

    private UnorderedMapping(UnorderedMapping unorderedMapping) {
        super(unorderedMapping);
    }

    @Override
    protected Optional<? extends IngredientMapping<?>> tryMerge(IngredientMapping<?> other) {
        return this.tryMergeImpl(switch(other) {
            case UnorderedMapping unordered -> unordered;
            case OrderedMapping ordered -> new UnorderedMapping(ordered);
        });
    }

    @Override
    protected Optional<IngredientSet> getImpl(char token) {
        return this.ingredientPossibilities.get(token);
    }

    @Override
    protected UnorderedMapping copy() {
        return new UnorderedMapping(this);
    }

    @Override
    protected Optional<IngredientSet> mergeIngredients(
        IngredientSet ourPossibilities, IngredientSet theirPossibilities
    ) {
        final IngredientSet intersection = ourPossibilities.getIntersection(theirPossibilities);
        return intersection.isEmpty() ? Optional.empty() : Optional.of(intersection);
    }

    // Enforces idSubstringMap and whenever an IngredientSet is narrowed down to one possibility,
    //   removes that possibility from all other IngredientSet's
    // returns false iff all possibilities are eliminated for any mapped IngredientSet
    public boolean reduce(ImmutableMap<Character, Pair<String, Boolean>> idSubstrings) {
        for (final Character token : this.ingredientPossibilities.getTokens()) {
            final Pair<String, Boolean> substringAndNegate = idSubstrings.get(token);
            if (substringAndNegate == null) {
                continue;
            }

            final String substring = substringAndNegate.getFirst();
            final boolean negate = substringAndNegate.getSecond();
            final IngredientSet possibleIngredients = this.ingredientPossibilities.get(token).orElse(null);
            if (possibleIngredients == null) {
                continue;
            }

            possibleIngredients.removeIf(ingredient ->
                // contains substring but mustn't or lacks substring but needs it
                SourcePattern.ingredientContainsSubstring(ingredient.asIngredient(), substring) == negate
            );

            if (possibleIngredients.isEmpty()) {
                // eliminated all possibilities
                return false;
            }

            // true iff both:
            // - possibleIngredients has only one remaining ingredient -> propagation occurred
            // - propagation eliminated all possible ingredients from another IngredientSet
            final boolean propagationEliminatedAnIngredient = !possibleIngredients
                .getSingleIngredient()
                // narrowed down to 1 ingredient, remove that ingredient from other sets
                .map(singleIngredient -> this.propagateElimination(token, singleIngredient))
                .orElse(true);

            if (propagationEliminatedAnIngredient) {
                return false;
            }
        }

        return true;
    }

    private boolean propagateElimination(Character determinedToken, IngredientWrapper determinedIngredient) {
        final Queue<DeterminedIngredient> determinations = new LinkedList<>();
        determinations.add(new DeterminedIngredient(determinedToken, determinedIngredient));
        while (!determinations.isEmpty()) {
            final DeterminedIngredient determined = determinations.remove();

            for (final Character token : this.ingredientPossibilities.getTokens()) {
                if (token == determined.token) {
                    continue;
                }

                final IngredientSet possibleIngredients = this.ingredientPossibilities.get(token).orElse(null);

                if (possibleIngredients == null) {
                    continue;
                }

                if (possibleIngredients.remove(determined.ingredient)) {
                    if (possibleIngredients.isEmpty()) {
                        // eliminated all possibilities
                        return false;
                    }

                    possibleIngredients.getSingleIngredient().ifPresent(singleIngredient ->
                        // narrowed another set down to 1
                        determinations.add(new DeterminedIngredient(token, singleIngredient))
                    );
                }
            }
        }

        return true;
    }

    private record DeterminedIngredient(char token, IngredientWrapper ingredient) { }
}
