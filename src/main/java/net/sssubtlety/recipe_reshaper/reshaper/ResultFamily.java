package net.sssubtlety.recipe_reshaper.reshaper;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.util.Identifier;

import net.sssubtlety.recipe_reshaper.reshaper.pattern.result.ResultPattern;
import net.sssubtlety.recipe_reshaper.reshaper.mapping.IngredientMapping;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static net.sssubtlety.recipe_reshaper.util.StringUtil.lineJoin;

public final class ResultFamily {
    private final ImmutableList<ResultPattern<?>> patterns;

    private ResultFamily(ImmutableList<ResultPattern<?>> patterns) {
        this.patterns = patterns;
    }

    public Pair<Map<Identifier, RecipeHolder<?>>, List<Identifier>> generateRecipesAndRemovals(
        List<IngredientMapping<?>> ingredientMaps, Identifier reshaperId
    ) {
        final Map<Identifier, RecipeHolder<?>> recipesById = new HashMap<>();
        final List<Identifier> idsToRemove = new LinkedList<>();
        for (final ResultPattern<?> pattern : this.patterns) {
            final Pair<Map<Identifier, RecipeHolder<?>>, List<Identifier>> recipesAndRemovals =
                pattern.generateRecipesAndRemovals(ingredientMaps, reshaperId);

            recipesById.putAll(recipesAndRemovals.getFirst());
            idsToRemove.addAll(recipesAndRemovals.getSecond());
        }

        return new Pair<>(recipesById, idsToRemove);
    }

    public record Data(ImmutableList<ResultPattern.Assembler> assemblers) {
        public static final Codec<Data> CODEC = ResultPattern.Assembler.CODEC.codec().listOf()
            .xmap(ImmutableList::copyOf, Function.identity())
            .xmap(Data::new, Data::assemblers);

        /**
         * Creates a {@link ResultFamily} with the passed {@code tokens}.
         */
        public DataResult<ResultFamily> assemble(
            ImmutableSet<Character> tokens, ImmutableMap<Character, Ingredient> commonIngredients
        ) {
            final ImmutableList.Builder<ResultPattern<?>> patterns = ImmutableList.builder();
            final List<String> errors = new LinkedList<>();
            for (final ResultPattern.Assembler assembler : this.assemblers) {
                assembler.assemble(tokens, commonIngredients)
                    .ifError(error -> errors.add(error.message()))
                    .resultOrPartial()
                    .ifPresent(patterns::add);
            }

            if (errors.isEmpty()) {
                return DataResult.success(new ResultFamily(patterns.build()));
            } else {
                return DataResult.error(() -> lineJoin(errors));
            }
        }
    }
}
