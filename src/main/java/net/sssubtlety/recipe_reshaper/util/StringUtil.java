package net.sssubtlety.recipe_reshaper.util;

import net.minecraft.recipe.Ingredient;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.minecraft.util.Identifier.NAMESPACE_SEPARATOR;

import static java.lang.System.lineSeparator;

public final class StringUtil {
    private StringUtil() { }

    public static final String LINE_TAB = lineSeparator() + '\t';

    private static final Pattern NON_EMPTY_LINE_START = Pattern.compile("^(?=.)", Pattern.MULTILINE);

    private static final String DEFAULT_ID_DELIM = String.valueOf(NAMESPACE_SEPARATOR);
    private static final String PATH_ID_DELIM = "..";

    public static String toPrintableString(Ingredient ingredient) {
        return toString(ingredient, ", ", DEFAULT_ID_DELIM, "[", "]");
    }

    public static String toString(Ingredient ingredient, String itemDelim, String idDelim) {
        return toStringImpl(ingredient, idDelim, Collectors.joining(itemDelim));
    }

    public static String toString(
        Ingredient ingredient, String itemDelim, String idDelim, String prefix, String suffix
    ) {
        return toStringImpl(ingredient, idDelim, Collectors.joining(itemDelim, prefix, suffix));
    }

    private static String toStringImpl(
        Ingredient ingredient, String idDelim, Collector<CharSequence, ?, String> collector
    ) {
        return IngredientUtil.streamItems(ingredient)
            .map(Registries.ITEM::getId)
            .map(id -> toString(id, idDelim))
            .collect(collector);
    }

    public static String toString(Identifier id, String delim) {
        return id.getNamespace() + delim + id.getPath();
    }

    public static String idPathOf(Stream<Optional<Ingredient>> ingredients) {
        return ingredients
            .map(emptyableIngredient -> emptyableIngredient
                .map(StringUtil::idPathOf)
                .orElse("empty")
            )
            .collect(Collectors.joining("_/_"));
    }

    public static String idPathOf(Ingredient ingredient) {
        return toString(ingredient, "/", PATH_ID_DELIM);
    }

    public static String idPathOf(Identifier id) {
        return toString(id, PATH_ID_DELIM);
    }

    public static CharSequence tabIndent(CharSequence string) {
        return NON_EMPTY_LINE_START.matcher(string).replaceAll("\t");
    }

    public static String lineJoin(List<? extends CharSequence> lines) {
        return lines.stream().collect(Collectors.joining(lineSeparator()));
    }
}
