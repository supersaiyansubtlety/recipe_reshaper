package net.sssubtlety.recipe_reshaper.util;

import net.minecraft.recipe.Ingredient;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;

public class IngredientSet extends LinkedHashSet<IngredientWrapper> implements IngredientConvertible {
    public IngredientSet() {
        super();
    }

    public IngredientSet(Ingredient ingredient) {
        this();
        this.add(ingredient);
    }

    public IngredientSet(Collection<Ingredient> ingredients) {
        this();
        this.addAllIngredients(ingredients);
    }

    public IngredientSet(IngredientSet ingredientSet) {
        super(ingredientSet);
    }

    public boolean add(Ingredient ingredient) {
        return this.add(new IngredientWrapper(ingredient));
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean addAllIngredients(Collection<Ingredient> ingredients) {
        boolean changed = false;
        for (final Ingredient ingredient : ingredients) {
            changed |= this.add(ingredient);
        }

        return changed;
    }

    public IngredientSet getIntersection(IngredientSet other) {
        final IngredientSet intersection = new IngredientSet(this);
        intersection.retainAll(other);
        return intersection;
    }

    @Override
    public boolean remove(Object o) {
        if (o instanceof IngredientWrapper wrappedIngredient) {
            return super.remove(wrappedIngredient);
        } else if (o instanceof Ingredient ingredient) {
            return super.remove(new IngredientWrapper(ingredient));
        } else {
            return false;
        }
    }

    @Override
    public Optional<Ingredient> tryAsIngredient() {
        return this.getSingleIngredient()
            .map(IngredientWrapper::asIngredient);
    }

    public Optional<IngredientWrapper> getSingleIngredient() {
        return this.size() == 1 ? Optional.of(this.iterator().next()) : Optional.empty();
    }
}
