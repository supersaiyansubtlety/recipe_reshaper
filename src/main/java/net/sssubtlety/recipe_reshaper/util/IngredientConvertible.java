package net.sssubtlety.recipe_reshaper.util;

import net.minecraft.recipe.Ingredient;

import java.util.Optional;

public interface IngredientConvertible {
    Optional<Ingredient> tryAsIngredient();
}
