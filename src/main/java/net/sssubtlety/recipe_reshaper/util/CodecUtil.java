package net.sssubtlety.recipe_reshaper.util;

import net.sssubtlety.recipe_reshaper.mixin.accessor.ShapedRecipePatterDataAccessor;

import com.mojang.datafixers.util.Either;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;

import java.util.function.Function;

public final class CodecUtil {
    private CodecUtil() { }

    public static final Codec<Character> TOKEN_CODEC =
        ShapedRecipePatterDataAccessor.recipe_reshaper$getKEY_SYMBOL_CODEC();

    public static <T> MapCodec<T> withAlternative(MapCodec<T> codec, MapCodec<T> alternativeCodec) {
        return Codec.mapEither(codec, alternativeCodec)
            .xmap(Either::unwrap, Either::left);
    }

    public static <A, B> Function<A, DataResult<B>> successOf(Function<A, B> converter) {
        return converter.andThen(DataResult::success);
    }
}
