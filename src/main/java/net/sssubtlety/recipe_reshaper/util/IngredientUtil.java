package net.sssubtlety.recipe_reshaper.util;

import com.google.common.collect.ImmutableSet;

import net.minecraft.item.Item;
import net.minecraft.recipe.Ingredient;
import net.minecraft.registry.Holder;

import java.util.stream.Stream;

public final class IngredientUtil {
    private IngredientUtil() { }

    public static boolean areEquivalent(Ingredient left, Ingredient right) {
        return left == right || copyItems(left).equals(copyItems(right));
    }

    public static ImmutableSet<Item> copyItems(Ingredient ingredient) {
        return streamItems(ingredient).collect(ImmutableSet.toImmutableSet());
    }

    // Ingredient::getItems
    @SuppressWarnings("deprecation")
    public static Stream<Item> streamItems(Ingredient ingredient) {
        return ingredient.getItems().map(Holder::getValue);
    }
}
