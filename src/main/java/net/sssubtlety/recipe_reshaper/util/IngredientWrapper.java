package net.sssubtlety.recipe_reshaper.util;

import com.google.common.collect.ImmutableSet;
import net.minecraft.item.Item;
import net.minecraft.recipe.Ingredient;

import java.util.Optional;

import static net.sssubtlety.recipe_reshaper.util.IngredientUtil.copyItems;

/**
 * Wrapper for ingredient that delegates {@link Object#equals(Object) equals} and {@link Object#hashCode() hashCode}
 * to a {@link java.util.Set Set}{@code <}{@link Item}{@code >} so it has desirable behavior when contained in a
 * {@link java.util.Set Set}, {@link java.util.Map Map}, or other data structure that uses
 * {@link Object#equals(Object) equals} or {@link Object#hashCode() hashCode}.
 */
public class IngredientWrapper implements IngredientConvertible {
    private final Ingredient ingredient;
    private final ImmutableSet<Item> matchingItems;

    public IngredientWrapper(Ingredient ingredient) {
        this.ingredient = ingredient;
        this.matchingItems = copyItems(ingredient);
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof IngredientWrapper ingredientWrapper &&
            this.matchingItems.equals(ingredientWrapper.matchingItems);
    }

    @Override
    public int hashCode() {
        return this.matchingItems.hashCode();
    }

    @Override
    public Optional<Ingredient> tryAsIngredient() {
        return Optional.of(this.asIngredient());
    }

    public Ingredient asIngredient() {
        return this.ingredient;
    }
}
