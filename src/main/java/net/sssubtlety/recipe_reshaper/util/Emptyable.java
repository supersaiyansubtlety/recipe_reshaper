package net.sssubtlety.recipe_reshaper.util;

import java.util.Optional;

// just a layer around an optional to eliminate confusion from Optional<Optional<?>>
@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public final class Emptyable<T> {
    public static final Emptyable<?> EMPTY = new Emptyable<>(Optional.empty());

    public static <T> Emptyable<T> of(T value) {
        return of(Optional.of(value));
    }

    public static <T> Emptyable<T> of(Optional<T> optional) {
        return optional.isPresent() ?
            new Emptyable<>(optional)
            : empty();
    }

    @SuppressWarnings("unchecked")
    public static <T> Emptyable<T> empty() {
        return (Emptyable<T>) EMPTY;
    }

    private final Optional<T> optional;

    private Emptyable(Optional<T> optional) {
        this.optional = optional;
    }

    public Optional<T> get() {
        return this.optional;
    }
}
