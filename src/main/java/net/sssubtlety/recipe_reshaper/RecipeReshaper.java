package net.sssubtlety.recipe_reshaper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RecipeReshaper {
	public static final String NAMESPACE = "recipe_reshaper";

	public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);
}
