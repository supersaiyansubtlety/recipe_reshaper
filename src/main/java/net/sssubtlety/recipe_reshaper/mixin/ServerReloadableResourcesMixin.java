package net.sssubtlety.recipe_reshaper.mixin;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.llamalad7.mixinextras.injector.ModifyReturnValue;

import net.minecraft.feature_flags.FeatureFlagBitSet;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.registry.HolderLookup;
import net.minecraft.registry.LayeredRegistryManager;
import net.minecraft.registry.RegistryKey;
import net.minecraft.resource.ResourceReloader;
import net.minecraft.server.ServerReloadableResources;
import net.minecraft.server.command.CommandManager;
import net.minecraft.util.Identifier;

import net.sssubtlety.recipe_reshaper.mixin.accessor.RecipeManagerAccessor;
import net.sssubtlety.recipe_reshaper.mixin.accessor.RecipeMapAccessor;
import net.sssubtlety.recipe_reshaper.reshaper.Reshaper;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mixin(value = ServerReloadableResources.class, priority = 250)
abstract class ServerReloadableResourcesMixin {
    @Unique
    private static RecipeHolder<?> withTypeSuffixedId(RecipeHolder<?> recipe) {
        final RegistryKey<Recipe<?>> oldKey = recipe.id();
        final Identifier newId = oldKey.getValue().extendPath("_" + recipe.value().getType());

        return new RecipeHolder<>(RegistryKey.of(oldKey.registryKey(), newId), recipe.value());
    }

    @Unique
    private Reshaper.Data.Manager reshaperManager;

    @Shadow
    @Final
    private RecipeManager recipes;

    @Inject(method = "<init>", at = @At("TAIL"))
    private void initFields(
        LayeredRegistryManager<?> layeredRegistryManager,
        HolderLookup.Provider lookupProvider, FeatureFlagBitSet featureFlags,
        CommandManager.RegistrationEnvironment environment,
        List<?> list, int level,
        CallbackInfo ci
    ) {
        this.reshaperManager = new Reshaper.Data.Manager(
            lookupProvider, Reshaper.Data.CODEC, Reshaper.Data.Manager.TYPE
        );
    }

    @ModifyReturnValue(method = "getReloaders", at = @At("RETURN"))
    private List<ResourceReloader> addReshaperManagerPreRecipes(List<ResourceReloader> reloaders) {
        final ArrayList<ResourceReloader> mutableReloaders = new ArrayList<>(reloaders);
        if (reloaders.get(2) == this.recipes) {
            mutableReloaders.add(2, this.reshaperManager);
            return List.copyOf(mutableReloaders);
        } else {
            for (int i = 0; i < mutableReloaders.size(); i++) {
                if (mutableReloaders.get(i) == this.recipes) {
                    mutableReloaders.add(i, this.reshaperManager);
                    return List.copyOf(mutableReloaders);
                }
            }

            throw new IllegalStateException("Could not find recipe manager!");
        }
    }

    @Inject(
        // method =
        //     "updateRegistryTags(" +
        //         "Lnet/minecraft/registry/DynamicRegistryManager;" +
        //         "Lnet/minecraft/registry/tag/TagManagerLoader$LoadResult;" +
        //     ")V",
        // method = "method_40421",
        method = "method_61248",
        at = @At("TAIL")
    )
    private void postTagUpdate(CallbackInfo ci) {
        final RecipeMapAccessor accessibleRecipeMap =
            (RecipeMapAccessor) ((RecipeManagerAccessor) this.recipes).recipe_reshaper$getRecipes();

        accessibleRecipeMap.recipe_reshaper$getByKey().forEach((key, recipeHolder) ->
            this.reshaperManager.checkRecipe(recipeHolder)
        );

        final Multimap<RecipeType<?>, RecipeHolder<?>> recipesByType =
            accessibleRecipeMap.recipe_reshaper$getByType();

        final Map<RegistryKey<Recipe<?>>, RecipeHolder<?>> recipesById =
            new HashMap<>((int) (recipesByType.size() * 1.5));

        for (final Map.Entry<RecipeType<?>, Collection<RecipeHolder<?>>> entry : recipesByType.asMap().entrySet()) {
            final RecipeType<?> type = entry.getKey();
            final Collection<RecipeHolder<?>> recipes = entry.getValue();

            final Iterable<RecipeHolder<?>> newRecipes =
                this.reshaperManager.generateAndRemoveRecipes(type, recipes);
            for (final RecipeHolder<?> newRecipe : newRecipes) {
                final RegistryKey<Recipe<?>> recipeKey = newRecipe.id();
                if (recipesById.containsKey(recipeKey)) {
                    // add type suffix to recipes with the same recipeId but different types
                    final RecipeHolder<?> conflict = recipesById.remove(recipeKey);

                    final RecipeHolder<?> uniqueConflictRecipe = withTypeSuffixedId(conflict);
                    recipesById.put(uniqueConflictRecipe.id(), uniqueConflictRecipe);

                    final RecipeHolder<?> uniqueNewRecipe = withTypeSuffixedId(newRecipe);
                    recipesById.put(uniqueNewRecipe.id(), uniqueNewRecipe);
                } else {
                    recipesById.put(recipeKey, newRecipe);
                }
            }
        }

        final ImmutableMultimap.Builder<RecipeType<?>, RecipeHolder<?>> recipesByTypeBuilder =
            ImmutableMultimap.builder();

        recipesById.forEach((id, recipe) ->
            recipesByTypeBuilder.put(recipe.value().getType(), recipe)
        );
        accessibleRecipeMap.recipe_reshaper$setByType(recipesByTypeBuilder.build());

        accessibleRecipeMap.recipe_reshaper$setByKey(ImmutableMap.copyOf(recipesById));
    }
}
