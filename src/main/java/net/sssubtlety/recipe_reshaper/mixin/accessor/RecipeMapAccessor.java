package net.sssubtlety.recipe_reshaper.mixin.accessor;

import com.google.common.collect.Multimap;

import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeHolder;
import net.minecraft.recipe.RecipeMap;
import net.minecraft.recipe.RecipeType;
import net.minecraft.registry.RegistryKey;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.Map;

@Mixin(RecipeMap.class)
public interface RecipeMapAccessor {
    @Accessor("byType")
    Multimap<RecipeType<?>, RecipeHolder<?>> recipe_reshaper$getByType();

    @Accessor("byType")
    @Mutable
    void recipe_reshaper$setByType(Multimap<RecipeType<?>, RecipeHolder<?>> byType);

    @Accessor("byKey")
    Map<RegistryKey<Recipe<?>>, RecipeHolder<?>> recipe_reshaper$getByKey();

    @Accessor("byKey")
    @Mutable
    void recipe_reshaper$setByKey(Map<RegistryKey<Recipe<?>>, RecipeHolder<?>> byKey);
}
