package net.sssubtlety.recipe_reshaper.mixin.accessor;

import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.ShapelessRecipe;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.List;

@Mixin(ShapelessRecipe.class)
public interface ShapelessRecipeAccessor {
    @Accessor("result")
    ItemStack recipe_reshaper$getResult();

    @Accessor("ingredients")
    List<Ingredient> recipe_reshaper$getIngredients();
}
