package net.sssubtlety.recipe_reshaper.mixin.accessor;

import net.minecraft.recipe.ShapedRecipePattern;

import com.mojang.serialization.Codec;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.util.List;

@Mixin(ShapedRecipePattern.Data.class)
public interface ShapedRecipePatterDataAccessor {
    @Accessor("PATTERN_CODEC")
    static Codec<List<String>> recipe_reshaper$getPATTERN_CODEC() {
        throw new IllegalStateException("Dummy method called!");
    }

    @Accessor("KEY_SYMBOL_CODEC")
    static Codec<Character> recipe_reshaper$getKEY_SYMBOL_CODEC() {
        throw new IllegalStateException("Dummy method called!");
    }
}
