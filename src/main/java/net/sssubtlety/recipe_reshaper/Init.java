package net.sssubtlety.recipe_reshaper;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.minecraft.util.Identifier;

import static net.fabricmc.fabric.api.resource.ResourceManagerHelper.registerBuiltinResourcePack;
import static net.fabricmc.fabric.api.resource.ResourcePackActivationType.DEFAULT_ENABLED;
import static net.sssubtlety.recipe_reshaper.RecipeReshaper.NAMESPACE;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        FabricLoader.getInstance().getModContainer(NAMESPACE).ifPresent(modContainer -> registerBuiltinResourcePacks(
            modContainer,
            "2x2_slabs-stairs-blocks",
            "2x2_logs_to_wooden_slabs_and_stairs",
            "2x2_panes_and_bars",
            "2x2_signs",
            "more_buttons",
            "conflict_resolution",
            "fixed_inconsistencies"
        ));
    }

    private static void registerBuiltinResourcePacks(ModContainer modContainer, String... paths) {
        for (final String path : paths) {
            registerBuiltinResourcePack(Identifier.of(NAMESPACE, path), modContainer, DEFAULT_ENABLED);
        }
    }
}
